# TODOs

## short term

-   [x] optimize width of gt tables for LaTeX/PDF output

    NOTES:

    -   [zero-width space](https://en.wikipedia.org/wiki/Zero-width_space) (`\u200B`) has no effect (doesn't result in a line break as one would expect)
    -   [thin space](https://en.wikipedia.org/wiki/Thin_space) (`\u2009`) has no effect (doesn't result in a line break as one would expect)
    -   `gt::tab_options(table.width)` has no effect at all
    -   `gt::tab_options(container.width)` has no effect at all
    -   `gt::cols_width(<colname> ~ gt::px(100), ...)` results in xelatex `compilation failed- error: Illegal unit of measure (pt inserted).` See
        <https://github.com/rstudio/gt/issues/1582>
    -   `gt::cols_width(<colname> ~ gt::pct(100), ...)` **works**! 🎉

-   get R obj from data prep and complete `method.qmd` TODOs:

    -   `comparison_participation_official_vs_survey`
    -   `participation_numbers`
    -   `decision_numbers`

-   test whether chapters really are [deeply merged](https://quarto.org/docs/projects/quarto-projects.html#metadata-merging) between multiple profiles

-   port `fokus_private/README.Rmd` to this repo's README

-   add `mode.{canton}.survey_institute` to supplemental date-specific FOKUS questionnaire data and use it in `imprint.qmd`

-   add short-long canton name mapping to pkg fokus (we could rely on `BFS::register_kt`)

-   split `input/2023-06-18_aargau.qmd` into 6 separate `input/2023-06-18_aargau_#` chapter files to get proper HTML output structure; this requires the
    contained R code to be overhauled since every chapter is rendered in isolation

-   Quarto-Profile dokumentieren: Standardprofil `_quarto.yaml` plus eines je Report plus je eines pro Format (HTML und PDF); letztere beiden Profil-Typen
    bilden je eine Profil-`group`; um einen Bericht zu rendern, müssen immer 2 Profile angegeben werden, eines je Typ.

-   falls die bestehende Profil-Aufteilung an Grenzen stossen sollte, kann mittels
    [`metadata-files`](https://quarto.org/docs/projects/quarto-projects.html#metadata-includes) fast beliebig feiner geteilt werden

-   find a way to display the author's ORCIDs in the imprint also for PDF output

-   [add suitable Hunspell dictionary](https://support.posit.co/hc/en-us/articles/200551916-Spelling-Dictionaries-in-the-RStudio-IDE) for Swiss German to our
    Rstudio Server and enable it for this project

## long term

-   create Quarto [Custom Format](https://quarto.org/docs/extensions/formats.html) extension with as much custom theming shared between RDB and FOKUS as
    possible

## contract work

### Formbricks

-   there's no single-use survey link overview [as described in the docs](https://formbricks.com/docs/link-surveys/single-use-links) for such surveys... bug?

-   add official API endpoints to generate [single-use survey IDs](https://formbricks.com/docs/link-surveys/single-use-links)

    NOTE: I'm not 100% sure this isn't already possible, I just couldn't find anything in the [API docs](https://formbricks.com/docs/developer-docs/rest-api)
    regarding this (the most relevant endpoints are *Create Survey* and *Update Survey*).

    when clicking on the button to "Regenerate single use survey link", the following (example) network request is sent:

    ``` sh
    curl 'https://forms.votelog.ch/environments/cltrvess70009fmsyl7w0dxkg/surveys/clwlb47cl0002djdabxx9lflj/summary' \
      --compressed \
      -X POST \
      -H 'Accept: text/x-component' \
      -H 'Accept-Encoding: gzip, deflate, br, zstd' \
      -H 'Referer: https://forms.votelog.ch/environments/cltrvess70009fmsyl7w0dxkg/surveys/clwlb47cl0002djdabxx9lflj/summary' \
      -H 'Next-Action: c8732ea04664c10c8245043962e3b7cbbf3092a9' \
      -H 'Next-Router-State-Tree: %5B%22%22%2C%7B%22children%22%3A%5B%22(app)%22%2C%7B%22children%22%3A%5B%22environments%22%2C%7B%22children%22%3A%5B%5B%22environmentId%22%2C%22cltrvess70009fmsyl7w0dxkg%22%2C%22d%22%5D%2C%7B%22children%22%3A%5B%22surveys%22%2C%7B%22children%22%3A%5B%5B%22surveyId%22%2C%22clwlb47cl0002djdabxx9lflj%22%2C%22d%22%5D%2C%7B%22children%22%3A%5B%22(analysis)%22%2C%7B%22children%22%3A%5B%22summary%22%2C%7B%22children%22%3A%5B%22__PAGE__%22%2C%7B%7D%2C%22%2Fenvironments%2Fcltrvess70009fmsyl7w0dxkg%2Fsurveys%2Fclwlb47cl0002djdabxx9lflj%2Fsummary%22%2C%22refresh%22%5D%7D%5D%7D%5D%7D%5D%7D%5D%7D%2Cnull%2Cnull%2Ctrue%5D%7D%5D%7D%2Cnull%2Cnull%2Ctrue%5D%7D%2Cnull%2Cnull%2Ctrue%5D' \
      -H 'Content-Type: text/plain;charset=UTF-8' \
      -H 'Origin: https://forms.votelog.ch' \
      -H 'Cookie: REDACTED \
      -H 'Sec-Fetch-Dest: empty' \
      -H 'Sec-Fetch-Mode: cors' \
      -H 'Sec-Fetch-Site: same-origin' \
      -H 'Priority: u=1' \
      -H 'TE: trailers' \
      --data-raw '["clwlb47cl0002djdabxx9lflj",false]'
    ```

    -   the prettified `Next-Router-State-Tree` header value reads:

        ``` json
        [
          "",
          {
            "children": [
              "(app)",
              {
                "children": [
                  "environments",
                  {
                    "children": [
                      [
                        "environmentId",
                        "cltrvess70009fmsyl7w0dxkg",
                        "d"
                      ],
                      {
                        "children": [
                          "surveys",
                          {
                            "children": [
                              [
                                "surveyId",
                                "clwlb47cl0002djdabxx9lflj",
                                "d"
                              ],
                              {
                                "children": [
                                  "(analysis)",
                                  {
                                    "children": [
                                      "summary",
                                      {
                                        "children": [
                                          "__PAGE__",
                                          {},
                                          "/environments/cltrvess70009fmsyl7w0dxkg/surveys/clwlb47cl0002djdabxx9lflj/summary",
                                          "refresh"
                                        ]
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      },
                      null,
                      null,
                      true
                    ]
                  }
                ]
              },
              null,
              null,
              true
            ]
          },
          null,
          null,
          true
        ]
        ```

    -   `cltrvess70009fmsyl7w0dxkg` is the environment ID and `clwlb47cl0002djdabxx9lflj` is the survey ID

    -   the request body with the raw JSON string `["clwlb47cl0002djdabxx9lflj",false]` seems crucial; the `false` or `true` seems to say whether the generated
        SUID is encrypted or not

    -\> find the survey's SUIDs in Formbricks' database and try generating a SUID directly via an API call; if the API call can be made without any extra GUI
    state knowledge, this should be enough for our MVP!

-   we need a way to define the length of the [single-use survey IDs](https://formbricks.com/docs/link-surveys/single-use-links) (they're too long for our use
    case)
    
    NOTE: as a workaround, we could implement some simple redirection logic from our own desired SUIDs to Formbricks' ones... but I dunno how exactly to achieve
    this with minimal effort.

-   we need access to the unencrypted SUIDs even if we're using encrypted links (which we ideally should) -\> first try to find them in Formbricks' database

-   implement survey welcome/landing page where people can enter their [single-use survey ID](https://formbricks.com/docs/link-surveys/single-use-links) (as an
    alternative to using the full link)

    NOTE: this is not critical since we can easily implement our own welcome/landing page

-   implement country + region (canton) detection *via IP* by parsing suitable HTTP header (`Cf-Connecting-IP`, `Fly-Client-IP`, `X-Azure-Socketip`, `X-Real-IP`
    or `X-Forwarded-For` etc.) and matching it with GeoIP data. this would be an alternative, more powerful approach to
    [#1892](https://github.com/formbricks/formbricks/pull/1892).

    -   parsing inspiration (in Go): <https://github.com/arp242/zhttp/blob/master/mware/realip.go>
    -   to create/update GeoIP data, we can use the [geoipupdate](https://github.com/maxmind/geoipupdate) CLI, see [this
        code](https://gitlab.com/votelog/apps/analytics/-/blob/main/entrypoint.sh?ref_type=heads#L3-12) for an example
