---
editor_options: 
  chunk_output_type: console
---

{{< include snippets/_load_data.qmd >}}

{{< include snippets/_load_plots.qmd >}}

# Ausgangslage

Die Aargauer Grossratswahlen vom 20. Oktober 2024 führten zu einer Stärkung der bürgerlichen Kräfte. Als grosse Siegerin ging dabei die SVP hervor (+3.6
Prozentpunkte). Mit einem Wähleranteil von 33.9 % knackte die wählerstärkste Partei des Kantons ihre bisherige Höchstmarke aus dem Jahre 2001 (33.5 %). Die
Volkspartei ist im Grossen Rat nun mit fünf zusätzlichen Sitzen vertreten. Die FDP konnte leicht zulegen (+0.7 Prozentpunkte, +1 Sitz) und erreichte einen
Wähleranteil von 15.4 %. Mit 12.9 % verharrte die Mitte ihrerseits auf dem Niveau von 2020 (+0.1 Prozentpunkte, keine Sitzveränderung). Die EDU konnte ihren
Wähleranteil auf 1.8 % erhöhen (+0.2 Prozentpunkte), was sich in einen Sitzgewinn niederschlug. Die vier bürgerlichen Parteien nehmen insgesamt 91 der 140
Grossratssitze ein, was einem Anstieg von sieben Mandaten entspricht. In der neuen Legislaturperiode verfügen SVP, FDP und EDU mit ihren insgesamt 73
Abgeordneten neu über eine absolute Mehrheit. Das linke Lager büsste dagegen vier Mandate ein und vereinigt noch 33 Sitze auf sich. Diese Verluste gingen
vollumfänglich auf das Konto der Grünen, deren Wähleranteil sich deutlich auf 7.4 % (-2.6 Prozentpunkte) reduzierte. Die SP erreichte ihrerseits 16.1 % (-0.5
Prozentpunkte, keine Sitzveränderung). Auch die GLP büsste leicht an Boden ein (-1 Prozentpunkt, -2 Sitze), wobei sie mit einem Wähleranteil von 8.2 % die Grünen
als fünfgrösste Partei des Kantons ablöste. Verluste musste ebenfalls die EVP hinnehmen (-0.3 Prozentpunkte, -1 Sitz). Ihr Wähleranteil belief sich auf 3.9 %.

{{< pagebreak >}}

# Beteiligung

## Beteiligung nach sozialen und politischen Merkmalen

Die Beteiligung bei den Aargauer Grossratswahlen 2024 erreichte lediglich 32.6 %, womit gegenüber 2020 ein leichter Rückgang zu verzeichnen war (-0.4
Prozentpunkte). Der erste Teil des vorliegenden Abschnitts beleuchtet die Wahlteilnahme nach sozialen Merkmalsgruppen. In Bezug auf das Geschlecht stellt sich
heraus, dass Männer weit häufiger partizipierten als Frauen (44 % vs. 27 %). Somit kann wie so oft bei Wahlen in der Schweiz von einem Geschlechtergraben die Rede
sein.

Ferner stiegen in der Tendenz die Partizipationsraten mit zunehmendem Alter. Wie die Abbildung 3.1 veranschaulicht, nahm in den beiden höchsten Alterskategorien
gut die Hälfte der stimmberechtigten Personen an den Grossratswahlen teil. Im Gegensatz dazu war dies bei den Bürgerinnen und Bürgern unter 50 Jahren nur bei
rund einem Viertel der Fall. Dazwischen lag die Wahlbeteiligung von Personen zwischen 50 und 59 Jahren (35 %).

Was die Ausbildung und das Einkommen anbetrifft, lassen sich ebenfalls gewohnte Muster der Wahlbeteiligung feststellen. Bei beiden Merkmalen ist von einem
positiven Zusammenhang zu berichten. Im Allgemeinen stieg demnach die Wahrscheinlichkeit einer Teilnahme mit zunehmendem Bildungsgrad und Einkommensniveau,
wobei sich dieser Zusammenhang in Bezug auf den höchsten Ausbildungsabschluss als stärker erwies als beim Haushaltseinkommen.

Schliesslich zeigt sich in Bezug auf die Religionszugehörigkeit, dass die Reformierten (41 %) und die Katholiken (38 %) häufiger an den Grossratswahlen teilnahmen
als die Konfessionslosen (32 %).

{{< include generated/snippets/_fig-bar-plot-participation-by-sociodemographics.qmd >}}

Wenden wir uns nun den politischen Einflussfaktoren der Wahlteilnahme zu. Aus Sicht der Parteien ist es von grosser Bedeutung, die jeweils eigene Anhängerschaft
zu mobilisieren. Aus der Abbildung 3.2 geht hervor, dass dies bei den Grossratswahlen 2024 unter den sechs grössten Aargauer Parteien der FDP (52 %) und der GLP
(51 %) am besten gelang. Rund die Hälfte der Sympathisierenden dieser beiden Parteien nahm an diesen Wahlen teil. Als nächstes folgten die Sympathisierenden der
SP (42 %), der Grünen und der Mitte (je 39 %). Hervorzuheben sind die gegenüber 2020 die deutlich tieferen Mobilisierungsraten im linken Lager. Damals beteiligten
sich 54 % (SP) bzw. 48 % (Grüne) der Parteianhängerschaften an den Grossratswahlen, was zu einen Grosserfolg der Grünen führte.

{{< include generated/snippets/_fig-bar-plot-participation-by-favored_party.qmd >}}

Der tiefste Wert ergab sich für jenes Segment, das der SVP nahesteht (35 %). Dies ist insofern bemerkenswert, als die Volkspartei die Grossratswahlen 2024
gewann. Somit wäre möglicherweise ein noch besseres Ergebnis in Reichweite gelegen. Allerdings ist darauf hinzuweisen, dass die SVP aufgrund des Profils ihrer
Anhängerschaft (u. a. vergleichsweise tiefes Bildungs- und Einkommensniveau) mehr Mühe als ihre Konkurrentinnen bekunden dürfte, die eigene Basis in Scharen an
die Urnen zu treiben. In Übereinstimmung mit dieser Sichtweise rangierte die SVP bereits vor vier Jahren in Bezug auf den Mobilisierungserfolg unter den sechs
grössten Parteien an letzter Stelle (37 %). In den Anhängerschaften der übrigen Parteien wurde derweil eine Wahlbeteiligung von sehr hohen 63 % erreicht. Dies ist
in erster Linie auf die Sympathisierenden der EDU (77 %) und der EVP (66 %) zurückzuführen. Schliesslich lässt sich die weitaus tiefste Partizipationsrate (14 %)
wenig überraschend bei jenen befragten Personen ermitteln, die sich mit keiner Partei identifizieren.

{{< include generated/snippets/_fig-bar-plot-participation-by-political_interest.qmd >}}

Nebst der Parteiidentifikation erweisen sich das politische Interesse, das politische Wissen, die Verbundenheit mit dem Kanton Aargau und das Vertrauen in den
Regierungsrat als Beteiligungstreiber. Als besonders erklärungskräftig stellt sich der Grad des politischen Interesses heraus, wie der Abbildung 3.3 zu
entnehmen ist. So nahmen Wahlberechtigte, die angaben, «sehr interessiert» an der Schweizer Politik zu sein deutlich häufiger am untersuchten Urnengang teil als
solche, die es gemäss Selbsteinschätzung überhaupt nicht sind (80 % vs. 3 %).

{{< include generated/snippets/_fig-bar-plot-participation-by-latent_expertise_5cat.qmd >}}

Ausserdem besteht ein positiver Zusammenhang zwischen dem politischen Wissen und der Wahlbeteiligung. Basierend auf drei Wissensfragen zum politischen System
des Kantons Aargau wurde eine latente Informiertheitsskala erstellt. Gruppiert man die befragten Personen in drei Hauptkategorien, so klafft eine Diskrepanz
zwischen den Bürgerinnen und Bürgern mit eher hohem Wissen einerseits und jenen mit mittlerem und eher geringem Wissen andererseits (vgl. die Abbildung 3.4). In
der Tat nahmen Erstgenannte rund zur Hälfte an den Grossratswahlen 2024 teil (48 %), während die Beteiligung in den übrigen zwei Kategorien ein lediglich halb so
hohes Niveau erreichte (25 % bzw. 23 %).

{{< include generated/snippets/_fig-bar-plot-participation-by-cantonal_attachment.qmd >}}

In Bezug auf die Verbundenheit mit dem Kanton Aargau gelangte eine Skala von 0 («gar nicht verbunden») bis 4 («stark verbunden») zur Anwendung. Die
Partizipationshäufigkeit stieg kontinuierlich an, wobei diese zwischen 15 % (Antwortoption 0) und 48 % (Antwortoption 4) lag, wie in der Abbildung 3.5 gezeigt
wird.

Die Höhe der Wahlbeteiligung hing schliesslich auch stark positiv vom Vertrauen in den Aargauer Regierungsrat ab. Wie aus der Abbildung 3.5 hervorgeht, nahmen
Bürgerinnen und Bürger mit sehr grossem Vertrauen rund viereinhalb so oft an den Grossratswahlen teil als jene, die der Kantonsregierung nur sehr tiefes
Vertrauen entgegenbringen (59 % vs. 13 %).

{{< include generated/snippets/_fig-bar-plot-participation-by-trust_in_cantonal_government_reduced.qmd >}}

## Gründe der Nicht-Teilnahme

Die vorliegende Befragung hat auch die individuellen Gründe der Nicht-Teilnahme erhoben. Jenen Befragten, die nicht an den Grossratswahlen 2024 teilgenommen
haben, wurden neun mögliche Abstinenzgründe vorgelegt. Dabei standen jeweils drei Antwortkategorien zur Auswahl («trifft zu», «trifft nicht zu» und «weiss
nicht»). Die Abbildung 3.7 stellt die abgefragten Gründe nach absteigender Häufigkeit dar. An erster Stelle liegt die Einschätzung, wonach keine Partei und
keine kandidierende Person überzeugten. 50 % der nicht-teilnehmenden Befragten wählten hier die Antwortoption «trifft zu». Danach folgten
Entscheidungsschwierigkeiten («ich konnte mich nicht entscheiden») und Überforderung («die Wahlen sind zu kompliziert») mit je 45 %.

Rund zwei von vier Nicht-Teilnehmenden führten Desinteresse («ich interessiere mich nicht für Politik»), Wirkungslosigkeit («ich bin der Meinung, dass Wahlen
ohnehin nichts ändern») sowie die Bedeutungslosigkeit der eigenen Stimme («ich bin der Meinung, dass es auf meine Stimme sowieso nicht ankommt») an. Das
Vergessen («ich habe vergessen teilzunehmen») und Verhinderungen («ich war verhindert») spielten eine untergeordnete Rolle (30 % bzw. 24 %). Dies ist insofern
bemerkenswert, als im Rahmen von Volksabstimmungen beide Gründe in aller Regel weit höher rangieren. Von insgesamt marginaler Bedeutung war schliesslich der
Grund, wonach die bevorzugte Partei bzw. kandidierende Person keine Chance auf eine Wahl hatte (11 %).

{{< include generated/snippets/_fig-bar-plot-non-participation-reasons.qmd >}}

{{< pagebreak >}}

# Meinungsbildung

## Entscheidungszeitpunkt

In Bezug auf die individuelle Meinungsbildung wird zunächst auf den Entscheidungszeitpunkt jener Befragten eingegangen, die an den Grossratswahlen 2024
teilgenommen haben. 50 % der Partizipierenden gaben an, bereits von Beginn weg gewusst zu haben, welche Partei sie wählen würden. Deren 30 % entschieden sich
einige Woche vor den Wahlen, 15 % einige Tage davor und die restlichen 5 % im letzten Moment.

Analysiert man die Entscheidungszeitpunkte der Wählerschaften der sechs grössten Parteien des Kantons Aargau, so tritt ein klares Muster zutage. Jene
Bürgerinnen und Bürger, die der SVP, der SP und der FDP ihre Stimme gaben, trafen ihre Wahl im Durchschnitt zu einem früheren Zeitpunkt als die Wählenden der
Mitte, der Grünen und der GLP. In Übereinstimmung mit diesem Befund sind die Wählerschaften der drei erstgenannten Parteien in der Gruppe der Spätentschlossenen
(Entscheid in den letzten Tagen vor der Wahl oder im letzten Moment) untervertreten (SVP 15 %, SP 16 %, SVP 18 %) - ganz im Gegensatz zu den drei letztgenannten
(GLP 30 %, Die Mitte 28 %, Grüne 24 %).

## Informationsgrundlagen

In diesem Abschnitt werden die Informationsquellen unter die Lupe genommen, die bei der Meinungsbildung der Bürgerinnen und Bürger eine Rolle spielten. Die
Analyse beschränkt sich wiederum auf die Wählenden. Die Abbildung 4.1 liefert Aufschluss über die Nutzung von zwölf abgefragten Medien. Es zeigt sich, dass die
offizielle Anleitung, die den kantonalen Wahlunterlagen beilag, am häufigsten genutzt wurde. Rund drei von fünf Wahlteilnehmende gaben an, diese
Informationsquelle gelesen zu haben (59 %).

Die restlichen elf wurden dagegen von einer Minderheit konsumiert. Kostenpflichtige Abonnementszeitungen und -magazine (41 %) sowie Gratiszeitungen und
kostenloser Online-Journalismus (39 %) erfreuten sich der grössten Beliebtheit, dicht gefolgt von Fernseh- und Radiosendungen (37 % und 34 %). Leserbriefe und
Online-Kommentare (29 %), Online-Wahlhilfen wie smartvote und Vimentis (26 %) sowie die Website des Kantons Aargau wurden immerhin von mindestens einem Viertel
der Partizipierenden genutzt (25 %).

Bei den sozialen Medien traf dies auf jede fünfte Person zu (21 %). Diesbezüglich ist indessen anzumerken, dass sich die Nutzung von Plattformen wie Facebook
oder X (ehemals Twitter) gegenüber den letzten Aargauer Grossratswahlen verdoppelte (2020: 11 %). Somit kann von einer zunehmenden Bedeutung die Rede sein. Im
Detail zeigt sich, dass in Bezug auf die Nutzung das Alter der Bürgerinnen und Bürger eine herausragende Rolle spielte. In der jüngsten Altersklasse (18–29
Jahre) waren die sozialen Medien bei 40 % der Wahlteilnehmenden von Bedeutung. Dieser Anteil nahm in den betagteren Kategorien kontinuierlich ab (30–39 Jahre:
37 %, 40–40 Jahre: 25 %, 50–50 Jahre: 16 %, 60–69 % 12 %) und erreichte unter den über 70-Jährigen noch 10 %.

Derweil wurden die kantonale Online-Wahlanleitung in leicht verständlicher Sprache und die easyvote-Wahlbroschüre von gut jeder zehnten teilnehmenden Person für
deren Meinungsbildung herangezogen (12 % und 10 %). Von untergeordneter Bedeutung waren schliesslich öffentliche Chatgruppen (5 %).

{{< include generated/snippets/_fig-bar-plot-information-source-usage-cantonal.qmd >}}

## Wahrnehmung der Kampagnenaktivitäten

Die Erhebungen von FOKUS Aargau enthalten jeweils auch eine Frage, die auf den direkten Kontakt mit Parteien und kandidierenden Personen abzielt. Diesbezüglich
wurden die Befragten mit fünf Kampagnenaktivitäten konfrontiert. Dabei wurden sie gebeten, für jede Aktivität anzugeben, ob sie während des Wahlkampfes in
Kontakt mit Parteien oder Kandidierenden kamen. Unter den Partizipierenden stellten sich Standaktionen als die häufigste Form der Kontaktaufnahme heraus. Wie
aus der Abbildung 4.2 ersichtlich ist, war dies bei 15 % der befragten Urnengängerinnen und Urnengänger der Fall. Immerhin 9 % gaben an, durch persönliche
Nachrichten und E-Mails kontaktiert worden zu sein. Von weit geringerer Bedeutung erwiesen sich dagegen Podiumsdiskussionen (4 %), Telefonaktionen (4 %) und
Hausbesuche (3 %). Zieht man den Vergleich zu den Grossratswahlen 2020 heran, zeigt sich insgesamt ein ähnliches Bild. Einzig die persönlichen Nachrichten und
E-Mails scheinen an Bedeutung eingebüsst zu haben (Rückgang von 15 % auf 9 %).

Es sei darauf hingewiesen, dass bei den Telefonaktionen erhebliche Unterschiede zwischen den Parteiwählerschaften ins Auge stechen. In der Tat gaben die
Wählenden von linken Parteien weit häufiger an, über diesen Kanal in Kontakt mit Parteien oder Kandidierenden gekommen zu sein. Während sich die entsprechenden
Anteile innerhalb der Wählerschaften der SP und der Grünen auf 16 % und 14 % beliefen, erreichten sie bei der Konkurrenz lediglich 3 % (FDP, Die Mitte und GLP)
oder 4 % (SVP). Diese Diskrepanz dürfte in erster Linie auf die Wahlkampfstrategie der SP zurückzuführen sein, die seit einigen Jahren konsequent auf
Telefonaktionen setzt.

{{< include generated/snippets/_fig-political-occasions.qmd >}}

{{< pagebreak >}}

# Wahlentscheid

## Wahlentscheid nach sozialen Merkmalen

In Bezug auf den Wahlentscheid nach sozialen Merkmalen (vgl. die Abbildung 5.1) sind zunächst geschlechtsspezifische Diskrepanzen festzustellen. Frauen
bevorzugten in der Tendenz linke Parteien, während Männer eher bürgerlich wählten. So erreichten die SP und die Grünen bei den Frauen einen kumulierten
Wähleranteil von 27 % - bei den Männern einen solchen von lediglich 21 %. Umgekehrt vereinigten die SVP und die FDP unter den teilnehmenden Männern mit 54 % eine
Mehrheit der Stimmen auf sich, während dies bei den Wählerinnen nicht der Fall war (43 %). Auf Stufe der einzelnen Parteien kann jedoch nur bei der SP (19 % bei
den Frauen vs. 14 % bei den Frauen) sowie bei der FDP (19 % bei den Männern vs. 10 % bei den Frauen) von einem Geschlechtergraben die Rede sein.

Auch beim Alter unterschied sich die Parteiwahl anlässlich der Grossratswahlen 2024 markant. Während das Durchschnittsalter der FDP-Wählerschaft bei hohen 57
Jahren lag, erreichte es innerhalb der Wählenden der Grünen 49 Jahre. Dazwischen lagen die Elektorate der Mitte (55 Jahre), der SP (55 Jahre), der SVP (52
Jahre) und der GLP (52 Jahre).

Im Detail zeigt sich, dass die Wählerschaft der SP eine u-förmige Altersstruktur aufwies. Auf überdurchschnittlichen Sukkurs konnte die Aargauer
Sozialdemokratie demnach in den ältesten und in den jüngsten Kategorien zählen. Die Grünen waren bei den Bürgerinnen und Bürgern über 70 Jahren deutlich
untervertreten (3 %), während die Grünliberalen in der Kategorie der 50-59-Jährigen auf grossen Anklang stiessen (13 %). Bei der Mitte erwiesen sich die
Unterschiede zwischen den Altersklassen als wenig ausgeprägt. Der Wähleranteil der FDP sank im Allgemeinen mit abnehmendem Alter – die einzige Ausnahme betraf
aber just die jüngste Kohorte der 18-29-Jährigen (16 %). Schliesslich ist zu betonen, dass sich im Gegensatz zu früheren Untersuchungen bei der SVP kein
Übergewicht der älteren Jahrgänge zeigte. Der Volkspartei ist es somit gelungen, vermehrt Jüngere anzusprechen. So ist hervorzuheben, dass sie gemäss der
vorliegenden Befragung den höchsten Wähleranteil in der zweitjüngsten Kategorie der 30-39-Jährigen erreichte (46 %).

Betrachtet man das Wählerprofil nach Bildungsniveau, so stechen übliche Muster ins Auge. Höhere Bildungsschichten waren in der SP, der GLP und der FDP
übervertreten und in den Wählerschaften der Mitte und der SVP untervertreten. Kein klarer Zusammenhang zwischen Bildungsstand und Parteiwahl war derweil bei den
Grünen ersichtlich.

In Bezug auf das Haushaltseinkommen der Wählenden zeigt sich für drei der sechs untersuchten Parteien ein deutliches Bild, wenn man die Quartile als Indikator
heranzieht. GLP und FDP zeichneten sich im Durchschnitt durch wohlhabende Wählerschaften aus, während bei den Grünen das Gegenteil der Fall war. Kein klares
Muster präsentierte sich hingegen bei der SP, der Mitte und der SVP.

{{< include generated/snippets/_fig-party-votes-cantonal-proportional-election-1-by-sociodemographics.qmd >}}

Wie haben es die verschiedenen Aargauer Parteiwählerschaften mit der Religion? Auf den ersten Blick stechen drei Erkenntnisse ins Auge: Die SP punktete bei den
Konfessionslosen (21 %), Die Mitte bei den Katholiken (22 %) und die FDP bei den Reformierten (21 %). Erwähnenswert ist auch der Umstand, dass die SVP unter den
Reformierten leicht untervertreten (28 %) war und sich bei den Katholiken (34 %), den Konfessionslosen (38 %) sowie bei den Angehörigen anderer Religionen (36 %)
einer grösseren Beliebtheit erfreute.

## Wahlentscheid nach politischen Merkmalen

### Problembewusstsein und Wahlentscheid

Vor allem in Bezug auf die Veränderungen der Wähleranteile ist jeweils die Themenkonjunktur von grosser Bedeutung. So konnte der Gewinn der Grünen und der
Grünliberalen vor vier Jahren auf die allgegenwärtige Debatte über die Folgen des Klimawandels in Zusammenhang gebracht werden. Welche Themen brannten den
Aargauerinnen und Aargauer 2024 unter den Nägeln? Dazu wurden die Befragten von FOKUS Aargau gebeten, das aus ihrer Sicht wichtigste politische Problem
anzugeben.

Aus der Abbildung 5.2 geht auf unmissverständliche Weise hervor, dass zwei Themen im Zentrum standen – Gesundheit und Einwanderung. Ersteres wurde von 18 % aller
befragten Personen erwähnt und kann u. a. mit der Ende September angekündigten Erhöhungen der Krankenkassenprämien in Zusammenhang gebracht werden. Der
Themenbereich der Einwanderung erreichte gar 23 %, wenn die Antwortkategorien «Ausländerinnen und Ausländer/Personenfreizügigkeit/Zuwanderung» (13 %) und
«Flüchtlinge/Asyl» (10 %) zusammengefasst werden. Umweltthemen (inkl. Klimawandel) wurden von 10 % der Befragten als wichtigstes Problem angekreuzt. Somit fiel
die Brisanz dieser Thematik gegenüber der «grünen Welle» vor vier Jahren deutlich zurück. Von einiger Bedeutung war auch die Europapolitik der Schweiz (9 %).

{{< include generated/snippets/_fig-bar-plot-main-political-issue-only-voters.qmd >}}

Die Abbildung 5.3 stellt dar, welche Problemthemen die Parteiwahl strukturierten. Etwas mehr als zwei Drittel jener Bürgerinnen und Bürger, die sich in erster
Linie über die Einwanderung besorgt zeigten, wählten die SVP (67 % bei der Zuwanderung und 72 % im Themenbereich der Asylpolitik). Somit erzielte die Volkspartei
in diesen Segmenten einen doppelt so hohen Wähleranteil als im Gesamtelektorat. Mit Ausnahme der FDP im Bereich der Zuwanderung (18 %), waren die restlichen
Parteien in diesen Wählergruppen deutlich untervertreten.

In Bezug auf das Gesundheitswesen erweisen sich die Unterschiede als weit weniger ausgeprägt. Ins Auge sticht jedoch der äusserst geringe Wähleranteil der
Grünen (4 %). Auch die Wählerschaft der SVP war unter jenen Teilnehmenden, die das Gesundheitswesen als grösstes Problem betrachteten, deutlich untervertreten
(25 %). Nur unwesentlich erhöhte Wähleranteile verzeichneten hingegen die SP (20 %), die FDP (18 %), Die Mitte (16 %) und die GLP (10 %).

Betrachtet man jene Wählende, die sich primär um die Umwelt Sorge machten, so fällt auf, dass in deutlich erhöhtem Ausmass jene der SP (37 %), der GLP (24 %) und
der Grünen (19 %) Teil dieser Gruppe sind. Ganz im Gegensatz dazu belaufen sich die entsprechenden Anteile zu Gunsten der SVP (5 %) und der FDP (3 %) auf einem
sehr tiefen Niveau.

Was die Europapolitik anbetrifft, lässt sich festhalten, dass innerhalb der Wählerschaft der FDP (25 %) dieses Thema leicht überdurchschnittlich als Hauptproblem
der Schweiz angesehen wurde. Wer SVP oder Grüne wählte, bezeichnete demgegenüber nur vergleichsweise selten die Beziehungen zu Europa als wichtigstes
politisches Problem (20 % und 2 %).

{{< include generated/snippets/_fig-party-votes-cantonal-proportional-election-1-by-main_political_issue.qmd >}}

### Links-rechts-Selbsteinstufung und Wahlentscheid

Zahlreiche Wahlstudien haben ergeben, dass die ideologische Positionierung der Bürgerinnen und Bürger den Wahlentscheid stark beeinflusst. Dabei wird in aller
Regel auf eine Links-Rechts-Skala zurückgegriffen, die von 0 («ganz links») bis 10 («ganz rechts») reicht. Im Rahmen der vorliegenden Befragung beläuft sich der
Durchschnittswert auf dieser Skala unter den Wählenden an den Grossratswahlen 2024 auf 5.5 und somit leicht rechts von der Mitte. Nach Parteiwählerschaften
aufgeschlüsselt zeigt sich eine grosse Übereinstimmung mit den gängigen parteipolitischen Positionierungen. Die Wählerschaften der linken Parteien (SP und
Grüne) platzierten sich beide auf 3.1. Jene Stimmberechtige, die sich für die GLP entschieden, befinden sich etwas links der Mittelkategorie (4.3), während die
Wählenden der Mitte fast exakt (5.1) darauf zu liegen kamen. Klar im rechten Lager verortete sich dagegen die Wählerschaft der FDP (6.5) und jene der SVP (7.5)
Die Abbildung 5.4 zeigt im Detail, wie sich die befragten Personen einstuften, wobei wiederum die grössten Parteiwählerschaften berücksichtigt wurden (inkl.
Restkategorie, die alle anderen Parteien erfasst).

Wie wirkte sich nun die ideologische Selbsteinstufung auf die Parteiwahl aus? Linksaussen (0–2) dominierte die SP mit rund zwei Drittel der Stimmen, der Rest
der Stimmen ging hauptsächlich an die Grünen. Im Bereich der moderaten Linken (3–4) erwuchsen den linken Parteien Konkurrenz in erster Linie durch die GLP,
wobei diese beim Skalenwert 4 obenaus schwang (41 %). Die Mitte stellt sich – nomen est omen – im politischen Zentrum (5) als wählerstärkste Partei heraus (31 %).
Die FDP wurde ihrerseits am häufigsten von jenen Bürgerinnen und Bürgern gewählt, die sich auf der Antwortkategorie 6 platzierten (32 %), wobei sie diesen
Wähleranteil auch auf der nächsten Stufe (7) erreichte. Die SVP rangierte von 7 bis 10 auf dem ersten Platz. Dabei nahm ihre Vorherrschaft zu, je weiter rechts
sich die befragten Wahlteilnehmenden einordneten. So erreichte die Volkspartei ganz rechts (10) einen Wähleranteil von beachtlichen 86 %.

{{< include generated/snippets/_fig-stacked-area-chart-left_right_self_positioning-by-voting_decision_cantonal_proportional_election_1_party.qmd >}}

{{< pagebreak >}}

## Wählerwanderungen

Dieser Abschnitt ist den sogenannten Wählerwanderungen gewidmet. Diese beruhen auf einem Vergleich des individuellen Wahlverhaltens bei den Grossratswahlen 2024
mit jenem von vor vier Jahren. Das Hauptaugenmerk liegt auf den in der Abbildung 5.5 visualisierten Wählerströmen, in der alle befragten Personen (inkl.
Nicht-Teilnehmende) berücksichtigt werden. Dieser Bericht beschränkt sich aufgrund der vorhandenen Stichprobengrössen wiederum auf die sechs wählerstärksten
Parteien.

{{< include generated/snippets/_fig-voter-migration-cantonal-proportional-election-1.qmd >}}

Zunächst sticht ins Auge, dass das mit Abstand grösste Segment an keinen der beiden hier interessierenden Wahlen teilnahm. Dieses umfasst gemäss der
vorliegenden Auswertung beinahe drei von fünf Stimmberechtige (59 %). Jeweils 9 % des Aargauer Elektorats wurden neu mobilisiert (Abstinenz 2020 und Teilnahme
2024) und demobilisiert (Teilnahme 2020 und Abstinenz 2024). Der Anteil jener Stimmberechtigten, die an beiden letzten Grossratswahlen teilnahmen, beläuft sich
somit auf die restlichen 23 %.

Zunächst lässt sich bei allen sechs untersuchten Parteiwählerschaften ein hohes Ausmass an Treue feststellen. Der Anteil jener Wählerinnen und Wähler von 2020,
die auch 2024 der gleichen Partei ihre Stimme gab, unterschied sich jedoch in erheblichem Masse. Der SVP und der FDP gelang es am besten, ihre Wählerschaften
von vor vier Jahren bei der Stange zu halten. Der entsprechende Anteil betrug bei beiden Parteien 67 %. Im Gegensatz dazu wurden die Grünen lediglich von etwas
mehr als zwei von fünf Wählenden wiederum unterstützt (43 %). Zwischen diesen beiden Extremen lagen die SP (63 %), Die Mitte (50 %) und die GLP (48 %).

Ausserdem ist die Mobilisierung von bisher Nicht-Teilnehmenden von Bedeutung. Aufgrund der tiefen Wahlbeteiligung stellen Nicht-Teilnehmende für alle Parteien
grundsätzlich ein grosses Stimmenreservoir dar. Allerdings ist zu beachten, dass jeweils auch ein Teil der bisherigen Wählerschaft demobilisiert wird. Die
vorliegende Auswertung zeigt, dass sich unter dem Strich im linken Lager eine erhebliche Demobilisierung feststellen lässt. Der Nettoabfluss an bisherigen
Wählenden betrug gemessen am realisierten Wahlergebnis von 2020 bei der SP 9 % und bei den Grünen 8 %. Während die GLP eine leichte Demobilisierung von netto 2 %
hinnehmen musste, überwog bei den drei grossen bürgerlichen Parteien die Mobilisierung von Nicht-Teilnehmenden. Die Effekte der Nettozuflüsse beliefen sich bei
der FDP und der Mitte auf jeweils 6 % und bei der SVP auf 4 %.

Abschliessend werden Wählerverschiebungen zwischen den Parteien untersucht. Bei den Grossratswahlen 2024 holte sich unter den grössten Aargauer Parteien die SVP
am meisten Stimmen von der Konkurrenz. Bezogen auf den Wähleranteil von 2020 erreichte der Nettozufluss beachtliche 9 %. Der Volkspartei gelang es dabei, auf
Kosten aller anderen Partei zuzulegen. Die grössten Prozentanteile stammten von jenen Stimmberechtigten, die 2020 noch die FDP (23 %) und - erstaunlicherweise -
die Grünen (29 %) bevorzugten.

Ebenfalls einen deutlichen Nettozufluss wies die SP aus (7 %). Dieser ging zu 70 % auf das Konto der Grünen, was angesichts der ideologischen Nähe der beiden
Parteien nicht zu überraschen vermag. Bei der FDP hielten sich derweil die Zu- und Abflüsse die Waage (0 %). Erstere stellten sich als stark fragmentiert heraus
– die grössten Nettozuflüsse kamen aus den Reihen von ehemaligen Wählenden der EVP, der Mitte und der Grünen. Im Gegensatz dazu präsentierte sich bei den
Verlusten ein klares Bild. In der Tat wanderten die allermeisten Stimmen zu Gunsten der SVP ab.

Federn lassen mussten dagegen die restlichen drei grossen Aargauer Parteien. Bei der Mitte hielten sich die Verluste mit 4 % jedoch in Grenzen. Wie bei den FDP
fielen die Abwanderungen zur SVP stark ins Gewicht. Die Grünliberalen büssten ihrerseits unter dem Strich 8 % an die Konkurrenz ein. Die Verluste gingen dabei in
alle Richtungen. Ehemalige Wählende der GLP gaben ihre Stimmen am häufigsten der SVP, gefolgt von der SP und der Mitte. Mit Abstand den grössten Nettoverlust
wiesen indessen die Grünen auf (17 %). Davon profitierten allen voran die SP (47 %) und die SVP (45 %).  

Zusammenfassend lässt sich festhalten, dass aus der Analyse der Wählerwanderungen drei Faktoren für den Wahlsieg der SVP hervorgehen. Erstens schaffte es die
Volkspartei, zwei Drittel ihrer Wählenden von 2020 zu behalten. Zweitens gelang es ihr, mehr ehemalige Nicht-Teilnehmende für sich zu gewinnen als bisherige
Wählerinnen und Wähler zu verlieren. Und drittens konnten zahlreiche Stimmen von Stimmberechtigten geholt werden, die vor vier Jahren noch eine andere Partei
bevorzugten.

Genau umgekehrt verhielt es sich mit den Grünen. Nur eine Minderheit ihrer Wählerschaft von 2020 wählte sie nochmals, der Rest ging entweder nicht an die Urnen
oder bevorzugte eine andere Partei. Bei der GLP liessen sich die gleichen Tendenzen feststellen, allerdings war das Ausmass dieser drei negativen Effekte
jeweils weit weniger ausgeprägt.

Wie der SVP gelang es der FDP, einen hohen Anteil ihrer Wählerschaft bei Stange zu halten. Auch die Mobilisierung von Nicht-Wählenden funktionierte ansprechend.
Hingegen schafften es die Freisinnigen nicht, anderen Parteien Stimmen abzujagen. Letzteres brachte jedoch die SP zustande. Allerdings konnten die
Sozialdemokraten nicht verhindern, dass ein beträchtlicher Teil ihrer Wählerschaft demobilisiert wurde. Schliesslich ist bei der Mitte die geglückte
Mobilisierung von Nicht-Wählenden hervorzuheben. Zugleich gingen jedoch zahlreiche Stimmen an die Konkurrenz verloren. Dies legt den Schluss nahe, dass in Bezug
auf die Zusammensetzung des Elektorates der Mitte mehr in Bewegung gekommen ist als aufgrund des stabilen Wähleranteils angenommen werden konnte.

{{< pagebreak >}}
