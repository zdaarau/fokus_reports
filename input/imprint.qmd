# Impressum {.unnumbered}

Die FOKUS-`r stringr::str_to_sentence(canton)`-Studien untersuchen das Stimmverhalten bei kantonalen Abstimmungen und Wahlen. FOKUS
`r stringr::str_to_sentence(canton)` wird vom Swisslos-Fonds des Kantons `r stringr::str_to_sentence(canton)` finanziert. Die Erhebung der Daten erfolgt durch
die Firma DemoSCOPE, während die Analysen vom Zentrum für Demokratie Aarau (ZDA) durchgeführt werden. Die Schlussberichte sind einige Wochen nach dem
Abstimmungs- oder Wahltermin auf der Webseite [berichte.fokus.ag](https://berichte.fokus.ag/) abrufbar, weitere Analysen finden sich unter
[analysen.fokus.ag](https://analysen.fokus.ag/). Die den Studien zugrunde liegenden Fragebogen sind auf derselben Seite frei zugänglich.

ISSN: {{< meta book.issn >}}\
ISBN: {{< meta book.isbn >}}

#### Projektverantwortung {.unlisted .unnumbered}

Uwe Serdült `r if (isTRUE(ballot_date <= "2021-11-28")) "und Thomas Milic"`

#### Autoren der vorliegenden Studie {.unlisted .unnumbered}

```{r}
#| results: asis

config <- quarto::quarto_inspect(profile = quappo::cur_profiles())$config

quappo::collapse_authors(config = config,
                         reverse_first = FALSE,
                         orcid_style = ifelse(knitr::is_latex_output(),
                                              "none",
                                              "icon"),
                         sep2 = " und ",
                         last = " und ") |>
  cat()
```

#### Zitiervorschlag {.unlisted .unnumbered}

`r quappo::collapse_authors(config = config, sep2 = ", ", last = ", ")`. "{{< meta book.title >}}: {{< meta book.subtitle >}}."
{{< meta book.collection-title >}}, {{< meta book.collection-number >}}. {{< meta book.publisher-place >}}: {{< meta book.publisher >}},
`r fokus::phrase_date(report_date)`. `r basename(config$book[["site-url"]])`.

```{r}
#| eval: !expr 'knitr::is_latex_output()'
#| results: asis

cat("\\vfill",
    "\\begin{center}",
    paste0("\\footnotesize{Dieses Dokument wurde zuletzt aktualisiert am ", fokus::phrase_date(today), ".}"),
    "\\end{center}",
    sep = "\n")
```

```{r}
#| eval: !expr 'knitr::is_html_output()'
#| results: asis

cat("---",
    "",
    "::: custom-font-small",
    paste0("*Dieses Dokument wurde zuletzt aktualisiert am ", fokus::phrase_date(today), ".*"),
    ":::",
    sep = "\n")
```
