{{< include snippets/_load_data.qmd >}}

# Methodischer Steckbrief

```{r}
#| label: method-setup
#| include: false

max_weight_participation <- pal::safe_max(data_survey$weight_participation)
min_weight_participation <- pal::safe_min(data_survey$weight_participation)
max_weight_decision <- pal::safe_max(data_survey$weight_decision)
min_weight_decision <- pal::safe_min(data_survey$weight_decision)
```

## Die Datenerhebung

Die vorliegende FOKUS-Aargau-Studie unterscheidet sich in der Datenerhebung von den Vorgängerstudien, welche alle auf einer repräsentativen
Bevölkerungsstichprobe basierten, welche per Briefpost zur Teilnahme eingeladen wurde.

Dieses Mal wurden die Zielpersonen aus einer Kombination mehrerer kommerzieller Online-Panels rekrutiert, welche zusammen eine ausreichend hohe Zahl
stimmberechtigter Aargauer:innen enthalten, um auf akzeptable Fallzahlen zu kommen. Eine Grenze von 2'000 Teilnehmer:innen wurde als Ziel definiert, d. h. es
wurden vom Befragungsinstitut so lange zusätzliche Aargauer:innen[^method_2024-10-20-1] aus dem Panel-Pool zur Umfrage eingeladen[^method_2024-10-20-2], bis
dieses Ziel erreicht wurde. Dies war nach 10 Tagen der Fall, insgesamt nahmen `r nrow(data_survey)` nach Eigenangabe stimmberechtigte Aargauer:innen teil
innerhalb eines Zeitraumes von 11 Tagen vom 21. bis und mit 31. Oktober 2024. Für die Befragung stand nur ein Online-Fragebogen zur Verfügung, keine
Papierversion. Die mittlere Befragungsdauer betrug 12 Minuten. Befragte, welche 30 % dieser Median-Interviewdauer unterschritten, wurden aus Gründen der
Qualitätssicherung ausgeschlossen[^method_2024-10-20-3].

Die am Urnengang Teilnehmenden sind bei politischen Nachbefragungen üblicherweise übervertreten. Auch in der vorliegenden Studie betrug die Differenz zwischen
der tatsächlichen und der in der Umfrage erhobenen Partizipationsquote rund 40.1 Prozentpunkte. Die Differenz bezüglich Wahlentscheid ist indessen deutlich
geringer. In der Umfrage gaben 23 bzw. 12.9 Prozent an, die stärkste (SVP) und zweitstärkste Partei (SP) gewählt zu haben, während es am Urnengang vom
`{r} fokus::phrase_date(ballot_date)` in Tat und Wahrheit 33.9 und 16.1 Prozent waren.

[^method_2024-10-20-1]: Unter Berücksichtung von Zielquoten nach Alter und Geschlecht, so dass die am Ende resultierenden Teilnehmer:innen möglichst der
    tatsächlichen Verteilung dieser Merkmale in der Bevölkerung entsprechen. Dies ist ein pragmatischer Kompromiss, um ausreichend repräsentative Daten zu
    erheben.

[^method_2024-10-20-2]: Die Personen wurden jeweils einmalig zur Teilnahme eingeladen, auf Erinnerungen wurde verzichtet.

[^method_2024-10-20-3]: Insgesamt wurden dadurch 40 Befragte ausgeschlossen.

## Die Gewichtung {#sec-weighting}

Jede Bevölkerungsumfrage weist Verzerrungen auf. Diese Verzerrungen können aus dem Verfahren (zufälliger Stichprobenfehler, *sampling error*), dem
Stichprobenrahmen (*coverage error*) und aus der Stichprobenrealisierung (Interviewverweigerung, *non-response error*) resultieren. Entählt der Auswahlrahmen
etwa nicht alle Elemente der Grundgesamtheit oder unterscheiden sich die Umfrageteilnehmer/innen systematisch von den Umfrageverweiger/innen -- wie oft der
Fall[^method_2024-10-20-4] -- hat das zwangsläufig Stichprobenverzerrungen zur Folge. Um diese zu korrigieren, werden gemeinhin Gewichtungsverfahren eingesetzt.

Auch bei der vorliegenden Studie wurden Gewichtungsfaktoren verwendet. Das dabei eingesetzte Gewichtungsverfahren war ein
Kalibrationsverfahren[^method_2024-10-20-5], das *Iterative Proportional Fitting* (*IPF*, auch *Raking* oder *Raking Ratio* genannt). Mit einem bestimmten
Algorithmus[^method_2024-10-20-6] werden beim Raking die Randverteilungen zwischen Stichprobe und den bekannten Parametern der Grundgesamtheit durch ein
iteratives Vorgehen in Einklang gebracht.[^method_2024-10-20-7]

Der Erfolg eines Raking-Verfahrens ist im Wesentlichen davon abhängig, ob die folgende Annahme zutrifft: Die Respondenten *innerhalb der einzelnen Klassen*
einer Gewichtungsvariablen müssen stellvertretend für die Nichtrespondenten in denselben Klassen stehen. Am Beispiel des Mittelwertes als interessierende Grösse
bedeutet dies: $\bar{Y_r} = \bar{Y_n}$, wobei $r$ für die Gruppe der Respondenten innerhalb einer bestimmten Merkmalsgruppe steht (z. B. über 60-jährige Frauen)
und $n$ für die Nicht-Respondenten aus derselben Gruppe. Diese Annahme kann nicht überprüft werden. Aber gleichzeitig macht sie auf die grosse Bedeutung der
Auswahl der Gewichtungskriterien aufmerksam. Für unsere Studie wurde eine Angleichung nach den Kriterien Teilnahme und Entscheidverhalten (Grossratspartei und
Regierungsrat) vorgenommen.

<!-- LoF/LoT is added here since we don't include the bibliography -->

\clearpage
\newpage
\listoffigures
\newpage

::: {.content-visible when-meta="custom.has_tbls"}
\listoftables
\newpage
:::

[^method_2024-10-20-4]: So haben diesmal etwa 74 Prozent der Umfrageteilnehmer/innen gemäss Eigenangabe gewählt, während die tatsächliche Wahlbeteiligung nur
    bei knapp 34 % der stimmberechtigten Aargauer:innen liegt (exkl. Auslandschweizer/innen).

[^method_2024-10-20-5]: Die in der Literatur verwendete Terminologie ist leider nicht einheitlich. Ab und an wird das hier verwendete Verfahren auch generell
    als Poststratifikation bezeichnet. Darunter verstehen wir Gewichtungsverfahren, die eine Angleichung der Stichprobenwerte aller (kreuztabulierten)
    Gewichtungsklassen an deren bekannte Populationsverteilung vornehmen. Wir beschränken den Begriff der Poststratifikation auf Verfahren, bei denen
    Zellensummen (im Gegensatz zu Randsummen, vgl. Kalibration) angeglichen werden. Unter Kalibrierungsverfahren verstehen wir hingegen Adaptionstechniken, mit
    denen die Randverteilungen der realisierten Stichprobe an bekannte Randverteilungen in der Bevölkerung angeglichen werden. Der Unterschied zur
    Poststratifikation liegt darin, dass bei der Kalibration keine Schichtung in sich *gegenseitig ausschliessende* Strata vorgenommen wird. Mit anderen Worten:
    Es werden keine Sollvorgaben für einzelne Gewichtungszellen definiert, sondern lediglich für die Randsummen.

[^method_2024-10-20-6]: Die klassische IPF-Prozedur gleicht die Randsummen einer Stichprobe den vorgegebenen Randsummen iterativ nach folgendem Algorithmus an:
    $${\hat  {m}}_{{ij}}^{{(2\eta -1)}}={\frac  {{\hat  {m}}_{{ij}}^{{(2\eta -2)}}x_{{i+}}}{\sum _{{k=1}}^{J}{\hat  {m}}_{{ik}}^{{(2\eta -2)}}}}$$
    $${\hat  {m}}_{{ij}}^{{(2\eta )}}={\frac  {{\hat  {m}}_{{ij}}^{{(2\eta -1)}}x_{{+j}}}{\sum _{{k=1}}^{I}{\hat  {m}}_{{kj}}^{{(2\eta -1)}}}}$$

[^method_2024-10-20-7]: Für unsere Schätzung haben wir das R-Paket [*anesrake*](https://cran.r-project.org/package=anesrake) verwendet. *anesrake* erlaubt ein
    sogenanntes *Trimming* (auch *Truncating* genannt) der Gewichte. Gemeint ist damit eine «Plafonierung» der Gewichtungswerte, indem eine Obergrenze definiert
    wird. Generell wird dadurch, dass man Obergrenzen (und teilweise auch Untergrenzen) für die Gewichtungswerte festlegt, verhindert, dass einzelnen
    Beobachtungen extrem hohe Gewichtungswerte zugewiesen werden. Gleichzeitig wird dadurch auch eine Verringerung des MSE angestrebt. In der angewandten
    Forschung kursieren unterschiedliche Richtwerte dazu. Wir haben einen Maximalwert von 5 definiert, die tatsächlich errechneten Maximalgewichte betragen
    allerdings nur `r pal::round_to(max_weight_participation, to = 0.01)` (nach Teilnahme) bzw. `r pal::round_to(max_weight_decision, to = 0.01)` (nach
    Stimmentscheiden). Die kleinsten errechneten Gewichte kamen indes bei `r pal::round_to(min_weight_participation, to = 0.01)` (nach Teilnahme) bzw.
    `r pal::round_to(min_weight_decision, to = 0.01)` (nach Stimmentscheiden) zu liegen.
