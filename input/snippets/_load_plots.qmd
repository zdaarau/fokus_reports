```{r}
#| label: load-plots
#| include: false

# read in 'plots' object if it exists
path_plots_ <- path_plots()

if (fs::file_exists(path_plots_)) {
  plots <- readRDS(file = path_plots_)
}

rm(path_plots_)
```
