Die jüngsten Aargauer Grossratswahlen führten zu einer Stärkung der bürgerlichen Kräfte. Die SVP als grosse Siegerin konnte ihren Wähleranteil auf 33.9 %
steigern – ein historischer Höchststand. Die Grünen erlitten dagegen eine herbe Niederlage und verloren vier Sitze im Grossen Rat. Die SVP profitierte davon,
dass die Einwanderung den Wahlberechtigten unter den Nägeln brannte. 23 % der Wahlberechtigten erachteten diesen Themenbereich als wichtigstes politisches
Problem. Wer sich in erster Linie um das Kernthema der SVP sorgte, wählte zu 69 % die Volkspartei.

Ebenfalls von grosser Bedeutung waren die steigenden Krankenkassenprämien (18 %). Allerdings gelang es keiner der sechs grössten Aargauer Parteien, sich aufgrund
der Brisanz dieses Themas einen entscheidenden Vorteil zu verschaffen. In dieser Themenkonstellation schaffte es die SVP, ihre Basis bei der Stange zu halten:
Rund zwei Drittel ihrer Wählerschaft von 2020 blieben der Volkspartei treu. Zudem gelang es ihr, Nicht-Wählende zu mobilisieren und starke Zugewinne von anderen
Parteien zu realisieren – insbesondere von der FDP und überraschenderweise auch von den Grünen.

Bemerkenswerterweise stiess die SVP bei den Jüngeren auf viel Zuspruch. Anders als früher zeigt sich kein Übergewicht älterer Wählerschichten mehr. Bei den 30
bis 39-Jährigen erreichte die Volkspartei sogar ihren höchsten Wähleranteil. 46 % der Teilnehmenden dieser Alterskategorie gaben der Volkspartei ihre Stimme.

Die Grünen und auch die Grünliberalen litten ihrerseits darunter, dass der Klimawandel nicht mehr die politische Debatte dominierte. Ihr Kernthema und Treiber
der Erfolge von 2020 wurde nur noch von 9 % der Wählenden als wichtigstes Problem genannt. Die sogenannte «grüne Welle» ist also abgeebbt.

Den Grünen gelang es am wenigsten, ihre Wählerschaft von 2020 zu halten: Nur 43 % wählten sie erneut, der Rest nahm entweder nicht an den letzten Grossratswahlen
teil oder bevorzugte andere Parteien. Die Grünen verloren im gleichen Zug viele Wählenden an die SP und an die SVP.

Die GLP vermochte zwar anteilsmässig am meisten Spätentschlossene für sich zu gewinnen (30 %), büsste aber unter dem Strich 8 % ihrer früheren Wählerschaft an die
Konkurrenz ein. Diese Verluste gingen in verschiedene Richtungen– am häufigsten zur SVP, gefolgt von der SP und der Mitte.

Die SP konnte trotz zahlreicher Stimmen von ehemaligen Wählenden der Grünen insgesamt nicht zulegen. Dies lag in erster Linie daran, dass viele SP-Wählende von
2020 den Urnen fernblieben. Den Sozialdemokraten gelang es nicht, diese Verluste durch die Mobilisierung von bisher Nicht-Wählenden auszugleichen. So
resultierte gemessen am Wähleranteil von 2020 ein Nettoabfluss von hohen 9 %.

Bei der Mitte glückte dagegen die Mobilisierung von Nicht-Wählenden. Zugleich verlor sie jedoch überdurchschnittlich viele Stimmen an konkurrierende Parteien,
allen voran an die SVP. Dies legt den Schluss nahe, dass in Bezug auf die Zusammensetzung der Wählerschaft der Mitte mehr in Bewegung kam als es aufgrund des
stabilen Wähleranteils den Eindruck erweckte.

Die Wählerschaft der FDP wies mit 57 Jahren das höchste Durchschnittsalter auf. Wie die SVP schafften es die Freisinnigen, zwei Drittel ihrer Wählerschaft von
2020 zu halten. Zudem gelang unter dem Strich die Mobilisierung von Nicht-Wählenden. Darüber hinaus gewann die FDP auf Kosten der EVP, der Mitte und den Grünen
hinzu. Allerdings gingen viele Stimmen an die SVP verloren, wodurch sich die Zu- und Abflüsse insgesamt die Waage hielten.
