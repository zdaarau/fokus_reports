profile:
  group:
    - [ "print", "web" ]
    - [ "2024-10-20_aargau", "2023-06-18_aargau", "minimal" ]

project:
  execute-dir: file
  lib-dir: libs
  post-render: ../R/post-render.R
  pre-render: ../R/pre-render.R
  resources:
    - static
  type: book

bibliography: assets/bibliography.json
csl: assets/citation_style.csl
fig-cap-location: top
# TODO: create reprex for our CSL plus this option and report issue
# reference-location: margin
tbl-cap-location: top
lang: "de-CH"
# source: https://quarto.org/docs/authoring/create-citeable-articles.html#google-scholar
# TODO: submit issue about error when specified (for books? or when bib not provided via `citation:` key but sep file?)
# google-scholar: true

book:
  date-format: long
  page-footer:
    left: "This work is licensed under <a href='https://creativecommons.org/licenses/by-sa/4.0/' rel=noopener target=_blank><i class='bi bi-cc-circle'></i> Creative Commons Attribution-ShareAlike 4.0 International</a>"
    right: "Copyright © 2023 <a href='https://www.zdaarau.ch/' rel=noopener target=_blank>Centre for Democracy Studies Aarau (ZDA) at the <a href='https://www.uzh.ch/' rel=noopener target=_blank>University of Zurich</a>, Switzerland"
    border: false
    background: "#343a40"
  publisher: "Zentrum für Demokratie Aarau (ZDA)"
  publisher-place: "Aarau"
  collection-title: "FOKUS-Aargau-Berichte"
  # doi: "?"
  issn: "2624-7399"
  language: "de-CH"
  license: "CC BY-SA"
  type: report
  downloads:
    - pdf
  open-graph: true
  repo-actions:
    - edit
  repo-branch: main
  repo-subdir: input
  repo-url: https://gitlab.com/zdaarau/fokus_reports
  site-url: https://reports.fokus.ag/
  comments:
    hypothesis:
      # for supported options, see https://h.readthedocs.io/projects/client/en/latest/publishers/config.html
      openSidebar: false
      showHighlights: whenSidebarOpen
      theme: classic
  back-to-top-navigation: true
  page-navigation: true
  reader-mode: false
  search:
    copy-button: true
    location: sidebar
    type: textbox
  twitter-card:
    site: "zdaarau"
  # crossref:
  #   appendix-title: "Appendix"
  #   appendix-delim: ":"

format:
  html:
    anchor-sections: true
    appendix-cite-as:
      - bibtex
    css: assets/gt.css
    citation: true
    citations-hover: true
    citation-location: document
    email-obfuscation: javascript
    fig-format: svg
    footnotes-hover: true
    include-in-header:
      - assets/in-header.html
    keep-md: false
    keywords:
      - Centre for Research on Direct Democracy (C2D)
      - direct democracy
      - direkte Demokratie
      - Analyse
      - Studie
    link-external-filter: "^((?:https?:)\\/\\/(creativecommons\\.org|gitlab\\.com\\/zdaarau|orcid\\.org|([a-zA-Z]+\\.)?fokus\\.ag|www\\.uzh\\.ch|www\\.zdaarau\\.ch|localhost|127.0.0.1)|mailto:.+@fokus.ag$|file://)"
    link-external-icon: true
    link-external-newwindow: true
    html-math-method:
      method: katex
      url: assets/npm/node_modules/katex/dist/
    # cf. https://quarto.org/docs/output-formats/html-themes.html#theme-options
    theme:
      - litera
      - assets/custom_litera.scss
    toc-depth: 6
    toc-title: "Auf dieser Seite"
  pdf:
    # classoption:
    #   - twocolumn
    documentclass: scrreprt
    fig-align: center
    fig-format: pdf
    filters:
      # cf. https://stackoverflow.com/a/74137244/7196903
      - assets/remove_title.lua
    footnotes-pretty: true
    geometry:
      # reference: http://mirrors.ctan.org/macros/latex/contrib/geometry/geometry.pdf
      - centering=true
      - hscale=0.55
      - vscale=0.86
      # - heightrounded # uncomment this if an `Underfull \vbox` warning should occur
    hyperrefoptions:
      - linktoc=all
      - pdfwindowui
    include-in-header:
      - assets/in-header.tex
    keep-md: false
    keep-tex: false
    latex-clean: true
    link-citations: true
    mainfont: AlegreyaSans
    mainfontoptions:
      - Path=assets/fonts/ttf/AlegreyaSans/
      - Extension=.ttf
      - UprightFont=*-Regular
      - BoldFont=*-Bold
      - ItalicFont=*-Italic
      - BoldItalicFont=*-BoldItalic
    monofont: FiraCode
    monofontoptions:
      - Path=assets/fonts/ttf/FiraCode/
      - Extension=.ttf
      - UprightFont=*-Regular
      - BoldFont=*-Bold
    papersize: a4
    sansfont: BigShouldersDisplay
    sansfontoptions:
      - Path=assets/fonts/ttf/BigShouldersDisplay/
      - Extension=.ttf
      - UprightFont=*-Regular
      - BoldFont=*-Bold
    template: assets/template.latex
    toc-depth: 6
    linkcolor: "DarkSlateBlue"
    filecolor: "DarkSlateBlue"
    citecolor: "DarkSlateBlue"
    urlcolor: "DarkSlateBlue"
    toccolor: "DarkSlateBlue"

knitr:
  opts_chunk:
    crop: false
    echo: false
    # cf. https://statr.me/2014/07/showtext-with-knitr/; doesn't actually seem necessary (or picked up?)
    # fig.showtext: true
    message: false
    warning: false
    out.width: "100%"
    # R.options:
    #   some.option: "some value"
  opts_knit:
    progress: false

execute:
  # we don't (yet) activate [knitr caching](https://quarto.org/docs/reference/cells/cells-knitr.html#cache) [globally](https://quarto.org/docs/projects/code-execution.html#cache)
  cache: false

custom:
  has_tbls: true
