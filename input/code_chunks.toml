# ------
# common
# ------

[fig-attitude]
itr_vars = [
  { var_name = "attitude_e_voting" }
]
label = "fig-likert-plot-{var_name}"
fig-cap = "Einstellung zum Thema { fokus::var_title(var_name = var_name, ballot_date = ballot_date, canton = canton) }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'likert_plot_{var_name}')"

[fig-attitude-by-x]
itr_vars = [
  { var_name = "attitude_e_voting", var_name_by = "age_group" },
  { var_name = "attitude_e_voting", var_name_by = "questionnaire_channel" }
]
label = "fig-likert-plot-{var_name}-by-{var_name_by}"
fig-cap = "Einstellung zum Thema { fokus::var_title(var_name = var_name, ballot_date = ballot_date, canton = canton) } nach { fokus::var_title(var_name = var_name_by, ballot_date = ballot_date, canton = canton) }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'likert_plot_{var_name}_by_{var_name_by}')"

[fig-decision-moments]
label = "fig-bar-plot-decision-moments"
fig-cap = "Entscheidzeitpunkt der { ifelse('referendum' %in% fokus::ballot_types(ballot_date = ballot_date, canton = canton), 'Stimmenden', 'Teilnehmenden') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_decision_moments')"

[fig-decision-moments-per-lvl]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton)"
label = "fig-bar-plot-decision-moments-{lvl}"
fig-cap = "Entscheidzeitpunkt der { ifelse('referendum' %in% fokus::ballot_types(ballot_date = ballot_date, canton = canton), paste0('Stimmenden zu den ', fokus::phrase(term = 'lvl', vals = lvl, lang = 'de'), 'en Vorlagen'), 'Teilnehmenden') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_decision_moments_{lvl}')"

[fig-information-source-usage]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton)"
label = "fig-bar-plot-information-source-usage-{lvl}"
fig-cap = "Mediennutzungsraten { ifelse(ballot_type == 'election', paste0('für die ', fokus::phrase(term = 'lvl', vals = lvl, lang = 'de'), 'en Wahlen'), paste0('der Stimmenden für die ', fokus::phrase(term = 'lvl', vals = lvl, lang = 'de'), 'en Abstimmungsvorlagen')) }"
fig-column = "page"
column = "page"
out_height = "!expr \"if (knitr::is_html_output()) '700px' else NULL\""
body = "insert_plot(id = 'bar_plot_information_source_usage_{lvl}')"

[fig-main-political-issue]
label = "fig-bar-plot-main-political-issue"
fig-cap = "{ fokus::var_title('main_political_issue', ballot_date = ballot_date, canton = canton) }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_main_political_issue')"

[fig-main-political-issue-only-voters]
label = "fig-bar-plot-main-political-issue-only-voters"
fig-cap = "{ fokus::var_title('main_political_issue', ballot_date = ballot_date, canton = canton) } (nur materiell Wählende, d. h. ohne Leerstimmen)"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_main_political_issue_only_voters')"

[fig-non-participation-reasons]
label = "fig-bar-plot-non-participation-reasons"
fig-cap = "Gründe für die Nicht-Teilnahme am Urnengang"
fig-column = "page"
column = "page"
out_height = "!expr \"if (knitr::is_html_output()) '500px' else NULL\""
body = "insert_plot(id = 'bar_plot_non_participation_reasons')"

[fig-participation-by-sociodemographics]
label = "fig-bar-plot-participation-by-sociodemographics"
fig-cap = "Teilnahme nach soziodemografischen Merkmalen"
fig-column = "page"
column = "page"
out_height = "!expr \"if (knitr::is_html_output()) '1000px' else NULL\""
body = "insert_plot(id = 'bar_plot_participation_by_sociodemographics')"

[fig-participation-by-x]
itr_vars = [
  { var_name = "age_group" },
  { var_name = "cantonal_attachment" },
  { var_name = "favored_party" },
  { var_name = "housing_situation" },
  { var_name = "latent_expertise_5cat" },
  { var_name = "political_interest" },
  { var_name = "trust_in_cantonal_government_reduced" }
]
label = "fig-bar-plot-participation-by-{var_name}"
fig-cap = "Teilnahme nach { dplyr::case_when(var_name == 'political_interest' ~ 'Politischem Interesse', stringr::str_detect(var_name, fokus:::as_sym_part_regex('latent_expertise')) ~ 'politischen Kenntnissen', .default = fokus::var_title(var_name = var_name, ballot_date = ballot_date, canton = canton) %||% '') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_participation_by_{var_name}')"

[fig-x-by-y]
itr_vars = [
  { var_name = "left_right_self_positioning", var_name_by = "favored_party" },
  { var_name = "left_right_self_positioning", var_name_by = "favored_party_extended" },
  { var_name = "left_right_self_positioning", var_name_by = "voting_decision_cantonal_proportional_election_1_party" },
  { var_name = "left_right_self_positioning", var_name_by = "voting_decision_past_cantonal_proportional_election_1_party" },
  { var_name = "left_right_self_positioning", var_name_by = "hypothetical_voting_decision_cantonal_proportional_election_1_party" }
]
label = "fig-area-charts-{var_name}-by-{var_name_by}"
fig-cap = "{ fokus::var_title(var_name = var_name, ballot_date = ballot_date, canton = canton) } nach { fokus::var_title(var_name = var_name_by, ballot_date = ballot_date, canton = canton) }"
fig-column = "page"
column = "page"
out_height = "!expr \"if (knitr::is_html_output()) '700px' else NULL\""
body = "insert_plot(id = 'area_charts_{var_name}_by_{var_name_by}')"

[fig-x-by-y-stacked]
itr_vars = [
  { var_name = "left_right_self_positioning", var_name_by = "favored_party" },
  { var_name = "left_right_self_positioning", var_name_by = "favored_party_extended" },
  { var_name = "left_right_self_positioning", var_name_by = "voting_decision_cantonal_proportional_election_1_party" },
  { var_name = "left_right_self_positioning", var_name_by = "voting_decision_past_cantonal_proportional_election_1_party" },
  { var_name = "left_right_self_positioning", var_name_by = "hypothetical_voting_decision_cantonal_proportional_election_1_party" }
]
label = "fig-stacked-area-chart-{var_name}-by-{var_name_by}"
fig-cap = "{ fokus::var_title(var_name = var_name, ballot_date = ballot_date, canton = canton) } nach { fokus::var_title(var_name = var_name_by, ballot_date = ballot_date, canton = canton) }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'stacked_area_chart_{var_name}_by_{var_name_by}')"

# -----------------
# proposal-specific
# -----------------

[fig-proposal-argument-agreement-by-decision]
itr_vars_r = "fokus::combos_proposal_arguments(ballot_dates = ballot_date, cantons = canton, incl_side = TRUE, incl_argument_nr = TRUE)"
label = "fig-likert-plot-{side}-argument-{argument_nr}-{lvl}-proposal-{proposal_nr}-agreement-by-decision"
fig-cap = "Einverständnis der Stimmenden mit dem { fokus::phrase(term = 'side', vals = side, lang = 'de') }-Argument «{ fokus::proposal_argument(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, side = side, argument_nr = argument_nr, lang = 'de', type = 'short') }»"
fig-column = "page"
column = "page"
body = "insert_plot(id = glue::glue('likert_plot_{side}_argument_{argument_nr}_{lvl}_proposal_{proposal_nr}_agreement_by_decision'))"

[fig-proposal-argument-importance-by-decision]
itr_vars_r = "fokus::combos_proposal_arguments(ballot_dates = ballot_date, cantons = canton, incl_side = TRUE, incl_argument_nr = TRUE)"
label = "fig-likert-plot-{side}-argument-{argument_nr}-{lvl}-proposal-{proposal_nr}-importance-by-decision"
fig-cap = "Bedeutung für die Stimmenden des { fokus::phrase(term = 'side', vals = side, lang = 'de') }-Arguments «{ fokus::proposal_argument(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, side = side, argument_nr = argument_nr, lang = 'de', type = 'short') }»"
fig-column = "page"
column = "page"
body = "insert_plot(id = glue::glue('likert_plot_{side}_argument_{argument_nr}_{lvl}_proposal_{proposal_nr}_importance_by_decision'))"

[fig-proposal-arguments-agreement]
itr_vars_r = "fokus::combos_proposal_arguments(ballot_dates = ballot_date, cantons = canton, incl_side = FALSE)"
label = "fig-likert-plot-arguments-{lvl}-proposal-{proposal_nr}-agreement"
fig-cap = "Einverständnis der Stimmenden mit den Pro- und Kontra-Argumenten zu{ ifelse(fokus::proposal_name_gender(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short') == 'feminine', 'r', 'm') } { fokus::phrase_proposal_name_de(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short', case = 'dative') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = glue::glue('likert_plot_arguments_{lvl}_proposal_{proposal_nr}_agreement'))"

[fig-proposal-arguments-importance]
itr_vars_r = "fokus::combos_proposal_arguments(ballot_dates = ballot_date, cantons = canton, incl_side = FALSE)"
label = "fig-likert-plot-arguments-{lvl}-proposal-{proposal_nr}-importance"
fig-cap = "Wichtigkeit für die Stimmenden der Pro- und Kontra-Argumente zu{ ifelse(fokus::proposal_name_gender(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short') == 'feminine', 'r', 'm') } { fokus::phrase_proposal_name_de(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short', case = 'dative') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = glue::glue('likert_plot_arguments_{lvl}_proposal_{proposal_nr}_importance'))"

[fig-proposal-decision-moment]
itr_vars_r = "fokus::combos_proposals(ballot_dates = ballot_date, cantons = canton)"
label = "fig-bar-plot-decision-moment-{lvl}-proposal-{proposal_nr}"
fig-cap = "Entscheidzeitpunkt der Stimmenden { fokus::proposal_name_gender(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short') |> salim::add_definite_article_de(preposition = 'bei') } { fokus::phrase_proposal_name_de(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short', case = 'dative') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = glue::glue('bar_plot_decision_moment_{lvl}_{proposal_nr}'))"

[fig-proposal-main-motive]
itr_vars_r = "fokus::combos_proposal_main_motives(ballot_dates = ballot_date, cantons = canton)"
label = "fig-bar-plot-main-motive-{lvl}-proposal-{proposal_nr}-{type}"
fig-cap = "Hauptmotiv der { fokus::phrase(term = 'main_motive_type', vals = type, lang = 'de') }-Stimmenden für die { ifelse(type == 'yes', 'Annahme', 'Ablehnung') } de{ ifelse(fokus::proposal_name_gender(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short') == 'feminine', 'r', 's') } { fokus::phrase_proposal_name_de(ballot_date = ballot_date, lvl = lvl, canton = canton, proposal_nr = proposal_nr, type = 'short', case = 'genitive') }"
fig-column = "page"
column = "page"
body = "insert_plot(id = glue::glue('bar_plot_main_motive_{lvl}_proposal_{proposal_nr}_{type}'))"

# ----------------
# across proposals
# ----------------

[fig-proposals-difficulty]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton, incl_lvl = FALSE)"
label = "fig-bar-plot-difficulty-proposals"
fig-cap = "Verständnisschwierigkeit der Abstimmungsvorlagen für die Stimmenden"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_difficulty_proposals')"

[fig-proposals-difficulty-per-lvl]
itr_vars_r = "fokus::combos_proposals(ballot_dates = ballot_date, cantons = canton, incl_nr = FALSE)"
label = "fig-bar-plot-difficulty-{lvl}-proposals"
fig-cap = "Verständnisschwierigkeit der { fokus::phrase(term = 'lvl', vals = lvl, lang = 'de') }en Abstimmungsvorlagen für die Stimmenden"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_difficulty_{lvl}_proposals')"

[fig-proposals-importance]
itr_vars_r = "fokus::combos_proposals(ballot_dates = ballot_date, cantons = canton, incl_nr = FALSE)"
label = "fig-bar-plot-importance-{lvl}-proposals"
fig-cap = "Persönliche Bedeutung der { fokus::phrase(term = 'lvl', vals = lvl, lang = 'de') }en Abstimmungsvorlagen für die Stimmenden"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_importance_{lvl}_proposals')"

[fig-proposals-information-level]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton, incl_lvl = FALSE)"
label = "fig-bar-plot-information-level-proposals"
fig-cap = "Selbsteinschätzung der Informiertheit der Stimmenden über die Abstimmungsvorlagen"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_information_level_proposals')"

[fig-proposals-information-level-per-lvl]
itr_vars_r = "fokus::combos_proposals(ballot_dates = ballot_date, cantons = canton, incl_nr = FALSE)"
label = "fig-bar-plot-information-level-{lvl}-proposals"
fig-cap = "Selbsteinschätzung der Informiertheit der Stimmenden über die { fokus::phrase(term = 'lvl', vals = lvl, lang = 'de') }en Abstimmungsvorlagen"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_information_level_{lvl}_proposals')"

[fig-proposals-information-level-reduced]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton, incl_lvl = FALSE)"
label = "fig-bar-plot-information-level-reduced-proposals"
fig-cap = "Selbsteinschätzung der Informiertheit (reduzierte Skala) der Stimmenden über die Abstimmungsvorlagen"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_information_level_reduced_proposals')"

[fig-proposal-information-level-reduced-per-lvl]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton)"
label = "fig-bar-plot-information-level-reduced-{lvl}-proposals"
fig-cap = "Selbsteinschätzung der Informiertheit (reduzierte Skala) der Stimmenden über die { fokus::phrase(term = 'lvl', vals = lvl, lang = 'de') }en Abstimmungsvorlagen"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_information_level_reduced_{lvl}_proposals')"

[fig-proposals-latent-expertise]
label = "fig-bar-plot-latent-expertise-proposals"
fig-cap = "Vorlagenkenntnisse der Stimmenden"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_latent_expertise_proposals')"

[fig-proposals-latent-expertise-per-lvl]
itr_vars_r = "fokus::combos_ballot_types(ballot_dates = ballot_date, cantons = canton)"
label = "fig-bar-plot-latent-expertise-{lvl}-proposals"
fig-cap = "Vorlagenkenntnisse der Stimmenden"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_latent_expertise_{lvl}_proposals')"

[fig-proposals-latent-expertise_by_x]
itr_vars_r = "fokus::combos_proposals(ballot_dates = ballot_date, cantons = canton, incl_nr = FALSE) |> fokus::add_vars_to_combos(var_names = c('age_group', 'favored_party'))"
label = "fig-bar-plot-latent-expertise-{lvl}-proposals-by-{var_name}"
fig-cap = "Vorlagenkenntnisse der Stimmenden nach { fokus::var_title(var_name = var_name, ballot_date = ballot_date, canton = canton) }"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_latent_expertise_{lvl}_proposals_by_{var_name}')"

# -----------------
# election-specific
# -----------------

[fig-election-party-votes-by-x]
itr_vars_r = "fokus::combos_elections(ballot_dates = ballot_date, cantons = canton) |> fokus::add_vars_to_combos(var_names = c('favored_party', 'main_political_issue', 'main_political_issue_reduced'))"
label = "fig-party-votes-{lvl}-{prcd}-election-{election_nr}-by-{var_name}"
fig-cap = "Parteistimmen { fokus::election_name(ballot_date = ballot_date, canton = canton, lvl = lvl, prcd = prcd, election_nr = election_nr) } { clock::get_year(ballot_date) } nach { switch(var_name, main_political_issue = 'Problemwahrnehmung', main_political_issue_reduced = 'Problemwahrnehmung', fokus::var_title(var_name)) } (nur materiell Wählende, d. h. ohne Leerstimmen)"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_voting_decision_{lvl}_{prcd}_election_{election_nr}_party_by_{var_name}')"

[fig-election-party-votes-by-sociodemographics]
itr_vars_r = "fokus::combos_elections(ballot_dates = ballot_date, cantons = canton, prcds = 'proportional')"
label = "fig-party-votes-{lvl}-{prcd}-election-{election_nr}-by-sociodemographics"
fig-cap = "Parteistimmen { fokus::election_name(ballot_date = ballot_date, canton = canton, lvl = lvl, prcd = prcd, election_nr = election_nr) } { clock::get_year(ballot_date) } nach soziodemografischen Merkmalen (nur materiell Wählende, d. h. ohne Leerstimmen)"
fig-column = "page"
column = "page"
out_height = "!expr \"if (knitr::is_html_output()) '1000px' else NULL\""
body = "insert_plot(id = 'bar_plot_voting_decision_{lvl}_{prcd}_election_{election_nr}_party_by_sociodemographics')"

[fig-election-voter-migration]
itr_vars_r = "fokus::combos_elections(ballot_dates = ballot_date, cantons = canton, prcds = 'proportional')"
label = "fig-voter-migration-{lvl}-{prcd}-election-{election_nr}"
fig-cap = "Wählerwanderungen zwischen { fokus::election_name(ballot_date = ballot_date, canton = canton, lvl = lvl, prcd = prcd, election_nr = election_nr) } { clock::get_year(ballot_date) - 4L } und { clock::get_year(ballot_date) }"
fig-column = "screen"
column = "screen"
out_height = "!expr \"if (knitr::is_html_output()) '1200px' else NULL\""
body = "insert_plot(id = 'chord_diag_{lvl}_{prcd}_election_{election_nr}_migration')"

[fig-election-voter-migration-material]
itr_vars_r = "fokus::combos_elections(ballot_dates = ballot_date, cantons = canton, prcds = 'proportional')"
label = "fig-voter-migration-{lvl}-{prcd}-election-{election_nr}-material"
fig-cap = "Wählerwanderungen zwischen { fokus::election_name(ballot_date = ballot_date, canton = canton, lvl = lvl, prcd = prcd, election_nr = election_nr) } { clock::get_year(ballot_date) - 4L } und { clock::get_year(ballot_date) }, nur materiell Wählende"
fig-column = "screen"
column = "screen"
out_height = "!expr \"if (knitr::is_html_output()) '1000px' else NULL\""
body = "insert_plot(id = 'chord_diag_{lvl}_{prcd}_election_{election_nr}_migration_material')"

# ----------------
# across elections
# ----------------

[fig-elections-political-occasions]
itr_vars_r = "fokus::combos_elections(ballot_dates = ballot_date, cantons = canton, incl_prcd = FALSE)"
label = "fig-political-occasions"
fig-cap = "Kontakt mit Parteien und Kandidierenden anlässlich politischer Anlässe/Aktionen"
fig-column = "page"
column = "page"
body = "insert_plot(id = 'bar_plot_political_occasions')"
