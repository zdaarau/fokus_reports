{{< include snippets/_load_data.qmd >}}

# Methodischer Steckbrief

```{r}
#| label: method-setup
#| include: false

max_weight_participation <- pal::safe_max(data_survey$weight_participation)
min_weight_participation <- pal::safe_min(data_survey$weight_participation)
max_weight_decision <- pal::safe_max(data_survey$weight_decision)
min_weight_decision <- pal::safe_min(data_survey$weight_decision)
survey_mode <- fokus:::raw_qstnr_suppl_mode(ballot_date = ballot_date,
                                            canton = canton)
n_invitees <- format(survey_mode$n,
                     big.mark = "'")
has_print <- "print" %in% c(survey_mode$invitation, survey_mode$reminder)
has_print_invitation <- "print" %in% survey_mode$invitation
prose_print_letter <- ifelse(has_print_invitation,
                             "Schreiben",
                             "Erinnerungsschreiben")
prose_print_constraint <-
  survey_mode$constraints |>
  pal::when("reminder_print_1970" %in% . ~ " für Teilnehmer:innen mit Jahrgang 1970 und älter",
            ~ "")
```

## Die Datenerhebung

Die vorliegende FOKUS-Aargau-Studie unterscheidet sich in der Datenerhebung von den Vorgängerstudien, da bei der Datenerhebung zusätzlich noch ein Experiment
für ein anderes Projekt eingebaut worden ist. Für das Experiment wurden die Treatment Gemeinden Aarau und Safenwil und die jeweiligen Kontrollgemeinden Baden
und Muhen überrepräsentiert [@heimann2023Demoscan]. Zusätzlich wurde auch die Gemeinde Wohlen überrepräsentiert. Dieses sogenannte Oversampling wird durch ein
Designgewicht ausgeglichen (vgl. Kapitel \ref{sec-weighting}). Die Resultate dieses Experimentes sind nicht Teil dieses Berichts, können aber auf der Website
des [Zentrums für Demokratie Aarau](https://www.zdaarau.ch/de/forschung/publikationen/) heruntergeladen werden.

Als Auswahlrahmen der vorliegenden Erhebung diente das kantonale Einwohnerregister (ERS) des Kantons Aargau. Statistik Aargau zog eine stratifizierte
Zufallsstichprobe, sodass für alle Treatment und Kontrollgemeinden jeweils 2'500 Zielpersonen und aus den restlichen Aargauer Gemeinden 8'000 Zielpersonen
gezogen worden sind (20'500 insgesamt). Durch dieses Vorgehen wird in einem ersten Schritt (Auswahlrahmen) eine lückenlose Abdeckung der Zielpopulation
(Aargauer Stimmberechtigte) gewährleistet. Die Zielpersonen erhielten ein Einladungsschreiben per Post, welches einen Zugangscode für den Online-Fragebogen
enthielt.

Daneben lag dem Erinnerungsschreiben für Teilnehmer:innen mit Jahrgang 1958 und älter auch ein gedruckter Fragebogen bei. Diese Zielpersonen hatten somit die
Wahl zwischen dem Ausfüllen eines digitalen Fragebogens übers Internet und eines klassischen Fragebogens mit Stift und Papier.

## Die Stichprobe

Die Nettostichprobe umfasst insgesamt 2'309 Befragte, wovon weniger als ein Viertel den Print-Fragebogen (n = 383) und mehr als drei Viertel den
Online-Fragebogen(n = 1926) ausgefüllt haben.

Die mittlere Befragungsdauer der Online-Erhebung betrug 27.1 Minuten. Die am Urnengang Teilnehmenden sind bei politischen Nachbefragungen üblicherweise
übervertreten. Auch in der vorliegenden Studie betrug die Differenz zwischen der tatsächlichen und der in der Umfrage erhobenen Partizipationsquote rund 40
Prozentpunkte. Die Differenz bezüglich Stimmentscheid ist indessen deutlich geringer. In der Umfrage gaben 62 Prozent an, dem Ombudsgesetz bzw. 57 Prozent der
Verdichtung des S-Bahn Angebots und 49 Prozent der kantonalen Klimainitiative zugestimmt zu haben, während es am Urnengang vom 18. Juni 2023 in Tat und Wahrheit
49.9 bzw. 47.6 und 32.1 Prozent waren. Bei allen drei Vorlagen lag der Ja-Anteil in der erhobenen Stichprobe deutlich über dem tatsächlichen
Ja-Anteil.[^method_2023-06-18-1]

<!-- Auch die Stichprobenverteilungen der Merkmale Alter, Geschlecht und Bezirkszugehörigkeit entsprechen weitestgehend den entsprechenden Verteilungen der Stimmberechtigten in der Gesamtpopulation. -->

<!-- ```{r}  -->

<!-- #| label: tbl-survey-response  -->

<!-- #| eval: !expr 'exists("table_survey_response")'  -->

<!--   -->

<!-- table_survey_response  -->

<!-- ``` -->

[^method_2023-06-18-1]: Die Differenzen wurden auf Basis der «materiellen» Entscheide errechnet. «Materiell» meint in diesem Zusammenhang, dass entweder ein
    «Ja» oder ein «Nein» eingelegt wurde. Tatsächlich kann man natürlich auch leer einlegen. Die leer Einlegenden wurden bei der Ermittlung der Differenzen
    zwischen den tatsächlichen und den in der Umfrage erhobenen Entscheiden *nicht* berücksichtigt.

## Die Gewichtung {#sec-weighting}

Jede Bevölkerungsumfrage weist Verzerrungen auf. Diese Verzerrungen können aus dem Verfahren (zufälliger Stichprobenfehler, *sampling error*), dem
Stichprobenrahmen (*coverage error*) und aus der Stichprobenrealisierung (Interviewverweigerung, *non-response error*) resultieren. Eine Verzerrung, die dadurch
bedingt ist, dass der Auswahlrahmen nicht alle Elemente der Grundgesamtheit enthält, kann bei der vorliegenden Erhebung prinzipbedingt nicht auftreten. Denn das
kantonale Einwohnerregister ist eine vollständige Liste der Zielpopulation[^method_2023-06-18-2]. Nicht alle gemäss Auswahlplan vorgesehenen Befragten sind
indessen erreichbar bzw. nehmen auch tatsächlich teil. Die Ausschöpfungsquote der vorliegenden Erhebung beträgt beispielsweise 11.3 Prozent. 88.7 Prozent
konnten demnach nicht erreicht werden bzw. waren nicht bereit, an der Umfrage teilzunehmen. Unterscheiden sich die Umfrageteilnehmer/innen systematisch von den
Umfrageverweiger/innen -- wie oft der Fall[^method_2023-06-18-3] -- hat eine mangelnde Ausschöpfung Stichprobenverzerrungen zur Folge. Um diese zu korrigieren,
werden gemeinhin Gewichtungsverfahren eingesetzt.

Auch bei der vorliegenden Studie wurden Gewichtungsfaktoren verwendet. Das dabei eingesetzte Gewichtungsverfahren war ein
Kalibrationsverfahren[^method_2023-06-18-4], das *Iterative Proportional Fitting* (*IPF*, auch *Raking* oder *Raking Ratio* genannt). Mit einem bestimmten
Algorithmus[^method_2023-06-18-5] werden beim Raking die Randverteilungen zwischen Stichprobe und den bekannten Parametern der Grundgesamtheit durch ein
iteratives Vorgehen in Einklang gebracht.[^method_2023-06-18-6] Da die Befragten über eine stratifizierte Zufallsstichprobe rekrutiert worden sind, ist
zusätzlich ein Designgewicht in das Rakingverfahren eingeflossen, um für diese Strata zu kontrollieren.

Der Erfolg eines Raking-Verfahrens ist im Wesentlichen davon abhängig, ob die folgende Annahme zutrifft: Die Respondenten *innerhalb der einzelnen Klassen*
einer Gewichtungsvariablen müssen stellvertretend für die Nichtrespondenten in denselben Klassen stehen. Am Beispiel des Mittelwertes als interessierende Grösse
bedeutet dies: $\bar{Y_r} = \bar{Y_n}$, wobei $r$ für die Gruppe der Respondenten innerhalb einer bestimmten Merkmalsgruppe steht (z. B. über 60-jährige Frauen)
und $n$ für die Nicht-Respondenten aus derselben Gruppe. Diese Annahme kann nicht überprüft werden. Aber gleichzeitig macht sie auf die grosse Bedeutung der
Auswahl der Gewichtungskriterien aufmerksam. Für unsere Studie wurde eine Angleichung nach den Kriterien Teilnahme und Entscheidverhalten (bei allen sechs
Vorlagen auf kantonaler und nationaler Ebene) vorgenommen.

[^method_2023-06-18-2]: In der Praxis kommt es aufgrund der Zeitverzögerungen zwischen der Registeraktualisierung sowie der Stichprobenziehung einerseits und
    dem Versand unserer Einladungsschreiben andererseits dennoch zu einigen durch Umzüge, Todesfälle etc. bedingten Ausfällen, was allerdings bloss
    vernachlässigbar kleine Verzerrungen nach sich zieht.

[^method_2023-06-18-3]: So haben diesmal etwa 82 Prozent der Umfrageteilnehmer/innen gemäss Eigenangabe abgestimmt, während die tatsächliche Stimmbeteiligung
    nur bei 42.5 % der stimmberechtigten Aargauer/innentaten liegt (exkl. Auslandschweizer/innen).

[^method_2023-06-18-4]: Die in der Literatur verwendete Terminologie ist leider nicht einheitlich. Ab und an wird das hier verwendete Verfahren auch generell
    als Poststratifikation bezeichnet. Darunter verstehen wir Gewichtungsverfahren, die eine Angleichung der Stichprobenwerte aller (kreuztabulierten)
    Gewichtungsklassen an deren bekannte Populationsverteilung vornehmen. Wir beschränken den Begriff der Poststratifikation auf Verfahren, bei denen
    Zellensummen (im Gegensatz zu Randsummen, vgl. Kalibration) angeglichen werden. Unter Kalibrierungsverfahren verstehen wir hingegen Adaptionstechniken, mit
    denen die Randverteilungen der realisierten Stichprobe an bekannte Randverteilungen in der Bevölkerung angeglichen werden. Der Unterschied zur
    Poststratifikation liegt darin, dass bei der Kalibration keine Schichtung in sich *gegenseitig ausschliessende* Strata vorgenommen wird. Mit anderen Worten:
    Es werden keine Sollvorgaben für einzelne Gewichtungszellen definiert, sondern lediglich für die Randsummen.

[^method_2023-06-18-5]: Die klassische IPF-Prozedur gleicht die Randsummen einer Stichprobe den vorgegebenen Randsummen iterativ nach folgendem Algorithmus an:
    $${\hat  {m}}_{{ij}}^{{(2\eta -1)}}={\frac  {{\hat  {m}}_{{ij}}^{{(2\eta -2)}}x_{{i+}}}{\sum _{{k=1}}^{J}{\hat  {m}}_{{ik}}^{{(2\eta -2)}}}}$$
    $${\hat  {m}}_{{ij}}^{{(2\eta )}}={\frac  {{\hat  {m}}_{{ij}}^{{(2\eta -1)}}x_{{+j}}}{\sum _{{k=1}}^{I}{\hat  {m}}_{{kj}}^{{(2\eta -1)}}}}$$

[^method_2023-06-18-6]: Für unsere Schätzung haben wir das R-Paket [*anesrake*](https://cran.r-project.org/package=anesrake) verwendet. *anesrake* erlaubt ein
    sogenanntes *Trimming* (auch *Truncating* genannt) der Gewichte. Gemeint ist damit eine «Plafonierung» der Gewichtungswerte, indem eine Obergrenze definiert
    wird. Generell wird dadurch, dass man Obergrenzen (und teilweise auch Untergrenzen) für die Gewichtungswerte festlegt, verhindert, dass einzelnen
    Beobachtungen extrem hohe Gewichtungswerte zugewiesen werden. Gleichzeitig wird dadurch auch eine Verringerung des MSE angestrebt. In der angewandten
    Forschung kursieren unterschiedliche Richtwerte dazu. Wir haben einen Maximalwert von 5 definiert, die tatsächlich errechneten Maximalgewichte betragen
    allerdings nur `r pal::round_to(max_weight_participation, to = 0.01)` (nach Teilnahme) bzw. `r pal::round_to(max_weight_decision, to = 0.01)` (nach
    Stimmentscheiden). Die kleinsten errechneten Gewichte kamen indes bei `r pal::round_to(min_weight_participation, to = 0.01)` (nach Teilnahme) bzw.
    `r pal::round_to(min_weight_decision, to = 0.01)` (nach Stimmentscheiden) zu liegen.

## Zur Inferenz

Resultate von Bevölkerungsumfragen sind stets mit einer gewissen Unsicherheit behaftet. Bei Zufallsstichproben kann man diese Unsicherheit indessen angeben.
Getan wird dies in aller Regel, indem man für alle Statistiken auch das zugehörige *Konfidenzintervall* ausweist. Dieses Intervall gibt die Bandbreite an,
innerhalb welcher der wahre Wert in der Grundgesamtheit mit einer von vornherein festgelegten Wahrscheinlichkeit zu liegen kommt. Diese Wahrscheinlichkeit (auch
«Konfidenzniveau» genannt) haben wir auf 95 Prozent festgelegt («doppelter Standardfehler»). Die entsprechende Bandbreite informiert demnach darüber, in welchem
Prozentbereich der wahre Wert in der Grundgesamtheit mit 95-prozentiger Wahrscheinlichkeit zu liegen kommt. Das 95 %-Konfidenzintervall ist dabei vom
Stichprobenumfang ($n$) wie auch der Verteilung der Variablenwerte ($\hat{p} \cdot (1-\hat{p})$) abhängig.

$$p_{o,u}=\pm 1.96 \cdot \sqrt{\frac{\hat{p} \cdot (1-\hat{p})}{n}}$$

Dazu ein Beispiel: Gehen wir zunächst von einem ausgeglichenen Stimmenverhältnis (d. h. einem Anteil von 50 Prozent Ja-Stimmen und 50 Prozent Nein-Stimmen) und
einem Stichprobenumfang von 1'000 Befragten aus. In einem solchen Fall betrüge der Stichprobenfehler ±3.1 Prozentpunkte und das dazugehörige Konfidenzintervall
käme demnach zwischen 46.9 und 53.1 Prozent zu liegen. Mit anderen Worten: Der wahre Wert in der Grundgesamtheit aller Aargauer Stimmenden käme mit einer
95 %-Wahrscheinlichkeit zwischen 46.9 und 53.1 Prozent zu liegen. Dieser Zufallsfehler erhöht sich -- wie aus obiger Formel ersichtlich -- mit abnehmender
Befragtenzahl nach dem Wurzel-n-Gesetz (d. h. der Stichprobenfehler verändert sich umgekehrt proportional zur Quadratwurzel der Stichprobengrösse), verringert
sich indessen, je unausgeglichener das Stimmenverhältnis ist. In der Praxis bedeutet dies, dass sich der Stichprobenfehler vor allem bei kleinen Merkmalsgruppen
erheblich erhöhen kann, was in der Folge die statistische Aussagekraft der entsprechenden Resultate stark beeinträchtigt.
