---
output: pal::gitlab_document
---

# FOKUS survey reports

[![Netlify Status](https://api.netlify.com/api/v1/badges/a90a130e-2dcd-4241-a346-4464d0bd122d/deploy-status)](https://app.netlify.com/sites/report-fokus-ag/deploys)

Sources of the [FOKUS survey reports](https://fokus.ag/analysen/).

```{r}
#| label: init
#| include: false

knitr::opts_knit$set(root.dir = getwd())

library(rlang,
        include.only = "%|%")
library(magrittr,
        include.only = c("%>%", "%<>%", "%T>%", "%!>%", "%$%"))
```

## Published reports

```{r}
#| label: published
#| results: asis
#| echo: false

fokus::combos_ballot_types(incl_lvl = FALSE) |>
  purrr::map(\(x) {
    url <- glue::glue("https://report.fokus.ag/{x$ballot_date}/{x$canton}/")
    
    if (pal::is_http_success(url)) {
      glue::glue("- [FOKUS {stringr::str_to_title(x$canton)} {x$ballot_date}]({url})")
    } else {
      NULL
    }
  }) |>
  purrr::list_c(ptype = character()) |>
  pal::cat_lines()
```

## Workflow

### Load ballot date

`init()` allows to load the ballot-date-specific `survey_data`, `plots` and some convenience variables like `ballot_date` into the R environment. It's called
during [R session startup](.Rprofile) and loads the latest[^readme-1] survey's data by default. Specify `ballot_date` to load another survey, e.g.:

``` r
init(ballot_date = "2023-06-18")
```

[^readme-1]: Actually, the R option `fokus.ballot_date` is respected first and only if it's unset, the latest data is loaded (which is always the case for an
    interactive session).

### Render a report

To render a survey report to **PDF**, specify the `"print"` profile in combination with a ballot-date-specific profile and the `"pdf"` output format. To render
e.g. the FOKUS Aargau 2024-10-20 report, run (from the `input/` directory):

``` r
quarto::quarto_render(profile = c("2024-10-20_aargau", "print"),
                      output_format = "pdf")
```

To render a survey report to **HTML**, specify the `"web"` profile in combination with a ballot-date-specific profile and the `"html"` output format. To render
e.g. the FOKUS Aargau 2024-10-20 report, run (from the `input/` directory):

``` r
quarto::quarto_render(profile = c("2024-10-20_aargau", "web"),
                      output_format = "html")
```

### View a rendered report

To view the rendered[^readme-2] report *immediately*, use [`quappo::view_output()`](https://quappo.rpkg.dev/reference/view_output).

To view e.g. the FOKUS Aargau 2024-10-20 report in **PDF** format, run (from the `input/` directory):

``` r
quappo::view_output(profile = c("2024-10-20_aargau", "print"),
                    output_format = "pdf")
```

To view the FOKUS Aargau 2024-10-20 report in **HTML** format instead, run (from the `input/` directory):

``` r
quappo::view_output(profile = c("2024-10-20_aargau", "web"),
                    output_format = "html")
```

By default, the HTML output is opened in RStudio's viewer pane if available[^readme-3].

Note that on RStudio *Server*, it might happen from time to time that an error message `Connection refused` instead of the actual HTML version of the report is
displayed. While the exact cause of this issue is still unknown to us[^readme-4], it can simply be remedied via a [*hard refresh* of the web
browser](https://www.howtogeek.com/672607/how-to-hard-refresh-your-web-browser-to-bypass-your-cache/), after which `quappo::view_output()` works as expected
again.

Note that `quappo::view_output()` does *not* automatically re-render content on changes. Use [Quarto's preview
functionality](https://quarto-dev.github.io/quarto-r/reference/quarto_preview.html) if you'd like that instead.

[^readme-2]: Meaning the files under `output/{ballot_date}/{canton}/`.

[^readme-3]: Of course, the rendered HTML output can be accessed using any (reasonably up-to-date) web browser by opening <http://127.0.0.1:3456> (`3456` is the
    default port which can be changed via the [`port` argument](https://quappo.rpkg.dev/reference/view_output#arg-port)).

[^readme-4]: My best guess would be that it's related to some premature token expiration.

## Dependencies

### R packages

To provide a reproducible R environment including the exact version of all necessary R packages, we rely on
[renv](https://rstudio.github.io/renv/articles/renv.html) and use its [`"explicit"` snapshot
type](https://rstudio.github.io/renv/reference/snapshot.html#snapshot-type).

The project's renv snapshot state will be [restored](https://rstudio.github.io/renv/reference/restore.html) during R session initialization, i.e. when executing
the [`input/.Rprofile` file](input/.Rprofile), so no manual action is required by collaborators to bring their renv libraries up to date. Depending on the
extent of snapshot changes, this can take a considerable amount of time (up to several minutes). The following message displayed on the console will indicate
when renv project restoral is completed:

```         
✔ Restoring renv state done. Ready to go.
```

#### Install new package

In order to introduce a new R package dependency `PKG_NAME`, the following steps are required:

1.  Add `PKG_NAME` including possible remotes to the [`input/DESCRIPTION` file](input/DESCRIPTION).

2.  Run in an R console (from the `input/` directory):

    ``` r
    renv::install("PKG_NAME",
                  prompt = FALSE,
                  lock = TRUE)
    ```

    Then restart the R session.

#### Update packages

In order to update all renv package records, run the following in an R console (from the `input/` directory):

``` r
renv::update(exclude = "renv",
             prompt = FALSE)
renv::snapshot(prompt = FALSE)
```

Then restart the R session.

### Pandoc template

To update the Pandoc LaTeX template to the [latest version](https://gitlab.com/salim_b/pandoc/templates/), run (from the `input/` directory):

``` r
salim::pandoc_tpl(tpl = "quarto_mod.latex") |> brio::write_file(path = "assets/template.latex")
```

## Technical remarks

Managing R state in Quarto projects is cumbersome since the main Quarto environment where inline R code and YAML `!expr`essions are evaluated is separate from
the environment where knitr code chunks are evaluated. While we can set state for the latter via `knitr.opts_chunk.R.options` in Quarto profiles, this has no
effect on the former. Instead, we must rely on `.Rprofile` only, there's no way to set R options (or environment variables) in Quarto profiles *for Quarto
itself* (only for knitr).

To work around this shortcoming, we derive the `ballot_date` to be processed from the currently rendered Quarto profile in `.Rprofile`. This might look overly
complicated at first, but it's actually the cleanest solution we could find so far.

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).
