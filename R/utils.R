init <- function(ballot_date = pal::pkg_config_val(key = "ballot_date",
                                                   pkg = "fokus"),
                 canton = pal::pkg_config_val(key = "canton",
                                              pkg = "fokus"),
                 lang = "de",
                 load_data = TRUE,
                 load_plots = load_data) {

  # check args only if pkgs are available to ensure this fn works when renv isn't yet activated
  if (nzchar(system.file(package = "rlang"))) {
    rlang::arg_match0(arg = as.character(ballot_date),
                      values = as.character(fokus::all_ballot_dates),
                      arg_nm = "ballot_date")
  }
  if (nzchar(system.file(package = "checkmate"))) {
    checkmate::assert_string(canton)
    checkmate::assert_string(lang)
    checkmate::assert_flag(load_data)
    checkmate::assert_flag(load_plots)
  }

  ballot_date <- as.Date(ballot_date)

  # set basic convenience vars
  # NOTE: R opts defined via `knitr.opts_chunk.R.options` are only available within code chunks, not inline code, thus we define regular constants
  assign(x = "canton",
         value = canton,
         envir = globalenv())
  assign(x = "ballot_date",
         value = ballot_date,
         envir = globalenv())

  # generate figure and table chunk snippets (i.a. required by `quarto::quarto_inspect()`)
  # NOTE: we can't generate them only in the `pre-render.R` script since Quarto evaluates include directives *before* executing the pre-render script
  if (nzchar(system.file(package = "brio")) &&
      nzchar(system.file(package = "fs")) &&
      nzchar(system.file(package = "purrr")) &&
      nzchar(system.file(package = "quappo"))) {

    quappo::read_chunk_toml(path = path_repo("input/code_chunks.toml")) |>
      quappo::chunks() |>
      purrr::iwalk(\(chunk, lbl) {

        path_snippets <- path_repo("input/generated/snippets")
        fs::dir_create(path_snippets)
        brio::write_file(text = chunk,
                         path = fs::path(path_snippets, paste0("_", lbl),
                                         ext = "qmd"))
      })
  }

  # set more convenience vars
  if (nzchar(system.file(package = "clock"))) {

    assign(x = "today",
           value = clock::date_today(zone = "Europe/Zurich"),
           envir = globalenv())

    if (nzchar(system.file(package = "quarto")) &&
        nzchar(system.file(package = "pal")) &&
        nzchar(system.file(package = "stringr")) &&
        nzchar(system.file(package = "cli"))) {

      # NOTE: `quarto::quarto_inspect()` below requires tmp QMD snippets to already exist
      assign(x = "report_date",
             value =
               quarto::quarto_inspect(profile = paste0(ballot_date, "_aargau"))$config$book$date |>
               pal::when(. == "today" ~ today,
                         stringr::str_detect(., "\\d{4}-\\d{2}-\\d{2}") ~ clock::date_parse(.),
                         is.character(.) ~ cli::cli_abort("Parsing {.field date:} {.val {.}} is not implemented yet."),
                         ~ .),
             envir = globalenv())
    }
  }

  if (load_data) {
    assign(x = "data_survey",
           value = fokus::read_survey_data(ballot_date = ballot_date,
                                           canton = canton,
                                           lang = lang),
           envir = globalenv())
  }

  if (load_plots) {
    path_plots_ <- path_plots(ballot_date = ballot_date,
                              canton = canton)

    if (file.exists(path_plots_)) {
      assign(x = "plots",
             value = readRDS(file = path_plots_),
             envir = globalenv())
    }
  }

  invisible(ballot_date)
}

insert_plot <- function(id,
                        static = !knitr::is_html_output(),
                        htmlwidget_asis = FALSE,
                        inline_svg = TRUE) {

  checkmate::assert_string(id)
  checkmate::assert_flag(static)
  checkmate::assert_flag(htmlwidget_asis)
  checkmate::assert_flag(inline_svg)
  is_html <- knitr::is_html_output()

  if (static || !is_html) {

    path <- path_img(ballot_date = ballot_date,
                     canton = canton,
                     id = id,
                     file_type = ifelse(is_html,
                                        "svg",
                                        "pdf"))
    if (is_html && inline_svg) {
      result <- brio::read_file(path = path) |> knitr::asis_output()
    } else {
      result <- knitr::include_graphics(path = path)
    }
  } else {

    result <- plots[[id]]

    # add HTML-specific plotly layout config
    if (inherits(result, "plotly")) {
      result %<>% salim::plotly_layout()
    }

    if (htmlwidget_asis) {
      result %<>%
        knitr::knit_print() %>%
        knitr::asis_output()
    }
  }

  result
}

#' Root criterion detecting this repository's root
#'
#' [rprojroot][rprojroot-package]-conformant [root criterion][rprojroot::root_criterion] detecting this repository's root directory.
#'
#' @keywords internal
is_repo <- function() rprojroot::has_file(filepath = "input/fokus_reports.Rproj")

#' Test whether path is within this repository
#'
#' Tests whether `path` is at or beneath the root of this repository.
#'
#' @param path Filesystem path to test. A character scalar.
#' @param start Starting directory to compute the path relative to. A character scalar. Only relevant if `path` is a relative path.
#'
#' @return A logical scalar.
#' @family path
#' @export
is_within_repo <- function(path = ".",
                           start = ".") {

  pal:::has_root(path = fs::path_abs(start = start,
                                     path = path),
                 criterion = is_repo(),
                 check_parent_dirs = TRUE)
}

#' Get static image path
#'
#' Returns the filesystem path to the static image with the filename `id` (without filetype suffix).
#'
#' @inheritParams fokus::lvls
#' @param id Image identifier. At the same time the image filename (without filetype suffix). A character scalar.
#' @param is_html Whether or not to return the path to a static image optimized for HTML output (SVG). If `FALSE`, the path to a PDF image is returned.
#'
#' @return `r pkgsnip::return_lbl("path")`
#' @export
path_img <- function(id,
                     ballot_date = pal::pkg_config_val(key = "ballot_date",
                                                       pkg = "fokus"),
                     canton = pal::pkg_config_val(key = "canton",
                                                  pkg = "fokus"),
                     file_type = c("svg", "pdf")) {

  checkmate::assert_string(id)
  file_type <- rlang::arg_match(file_type)

  path_repo("input/generated/images/", ballot_date, canton, id,
            ext = file_type)
}

path_obj <- function(id,
                     ballot_date = pal::pkg_config_val(key = "ballot_date",
                                                       pkg = "fokus"),
                     canton = pal::pkg_config_val(key = "canton",
                                                  pkg = "fokus")) {
  checkmate::assert_string(id)

  path_repo("input/generated/r_objects/", ballot_date, canton, id,
            ext = "rds")
}

path_plots <- function(ballot_date = pal::pkg_config_val(key = "ballot_date",
                                                         pkg = "fokus"),
                       canton = pal::pkg_config_val(key = "canton",
                                                    pkg = "fokus")) {
  path_obj(id = "plots",
           ballot_date = ballot_date,
           canton = canton)
}

#' Construct a path to a location within the local `fokus_reports` Git repository clone
#'
#' Assembles a path to a subdirectory of this repository's root. By default, returns the path to the repository root directory itself.
#'
#' @inheritParams fs::path
#' @param ... Optional path components relative to the repository root directory.
#' @param .path_start Directory to start the search for the repository root directory from. Must be a subdirectory of the repository root or the path to the
#'   repository root directory itself. Defaults to the current [working directory][getwd].
#'
#' @return `r pkgsnip::return_lbl("path")`
#' @family path
#' @export
path_repo <- function(...,
                      ext = "",
                      .path_start = ".") {

  if (!is_within_repo(path = .path_start)) {
    cli::cli_abort("{.arg .path_start} ({.val { .path_start}}) is not inside the {.field fokus_reports} Git repository.")
  }

  is_repo() |>
    rprojroot::find_root(path = .path_start) |>
    fs::path(...,
             ext = ext)
}
