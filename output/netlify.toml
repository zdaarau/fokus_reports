# ------- HEADERS -------
# documentation: https://docs.netlify.com/routing/headers/#syntax-for-the-netlify-configuration-file
[[headers]]
for = "/*"
[headers.values]
# for a brief introduction into CSPs, see https://infosec.mozilla.org/guidelines/web_security#content-security-policy
# for an in-depth description, see [An Introduction to Content Security Policy](https://www.html5rocks.com/en/tutorials/security/content-security-policy/)
# for an up-to-date list of available policies, see
# - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
# - [OWASP CSP Cheat Sheet](https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/Content_Security_Policy_Cheat_Sheet.md)
# notes:
# - `connect-src data:` is required for asciinema (asciicast)
# - `connect-src https://t.counter.dev` is required for counter.dev analytics
# - `connect-src https://cdn.plot.ly` is required for Plotly maps
# - `object-src 'self';` has to be set to enable PDF preview in Chrome
# - `script-src https://fokus.ag` is required for counter.dev analytics
# - `default-src https://hypothes.is`, `script-src https://hypothes.is https://cdn.hypothes.is` and `style-src https://cdn.hypothes.is` are required for hypothes.is commenting
# - `script-src 'unsafe-inline'` is required for Quarto (external links)
# - `style-src 'unsafe-inline'` is required for Quarto
Content-Security-Policy = '''
  base-uri 'self';

  default-src https://hypothes.is;
  connect-src 'self' data: https://t.counter.dev https://cdn.plot.ly;
  font-src 'self' data:;
  img-src 'self' data:;
  manifest-src 'self';
  object-src 'self';
  script-src 'self' 'unsafe-inline' https://fokus.ag https://hypothes.is https://cdn.hypothes.is;
  style-src 'self' 'unsafe-inline' https://cdn.hypothes.is;

  form-action 'self';
  frame-ancestors 'none'
'''
Referrer-Policy = "strict-origin-when-cross-origin"
X-Content-Type-Options = "nosniff"
X-Frame-Options = "DENY"
X-XSS-Protection = "1; mode=block"

# ------- PLUGINS -------
# documentation: https://docs.netlify.com/integrations/build-plugins/#file-based-installation
# NOTE: For plugins which are installed via Netlify's UI as [integrations(https://app.netlify.com/plugins), certain settings must be configured via environment
#       variables instead of the config section below (e.g. `audits` for `netlify-plugin-lighthouse`).
[[plugins]]
package = "@netlify/plugin-lighthouse"
# set the plugin to run prior to deploy, failing the build if minimum thresholds aren't set (required for the report to be statically served)
[plugins.inputs]
# set the plugin to run prior to deploy, failing the build if minimum thresholds aren't set
# NOTES:
# - Must be set as a string, not as a boolean.
# - Must be enabled in order for the report to be statically served to the audit's `output_path`.
# - Disabling this results in the hostname to always equal to `DEPLOY_URL` regardless of the audit's `url`. `DEPLOY_URL`s have the [`X-Robots-Tag` HTTP header](https://http.dev/x-robots-tag) set, so `seo` score is worse than on production site.
fail_deploy_on_score_thresholds = "false"
[[plugins.inputs.audits]]
path = "2023-06-18/aargau/"
# set minimum thresholds for each report area
[plugins.inputs.thresholds]
performance = 0.9
accessibility = 0.8
best-practices = 0.9
seo = 0.7
# run the desktop instead of the mobile device audit
[plugins.inputs.settings]
preset = "desktop"
locale = "en"

# ------- REDIRECTS -------
# Redirect rules are processed [from top to bottom](https://docs.netlify.com/routing/redirects/#rule-processing-order), i.e. first matching one has priority
## redirect old PDF filename to new one
[[redirects]]
  from = "/2023-06-18/aargau/report.pdf"
  to = "/2023-06-18/aargau/fokus_aargau_report_2023-06-18.pdf"

[[redirects]]
  from = "/2023-06-18/aargau/fokus_aargau_report_2023_06_18.pdf"
  to = "/2023-06-18/aargau/fokus_aargau_report_2023-06-18.pdf"
